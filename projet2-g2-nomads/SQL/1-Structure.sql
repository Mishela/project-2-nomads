-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema nomads
-- -----------------------------------------------------
DROP DATABASE IF EXISTS `nomads`;
CREATE SCHEMA IF NOT EXISTS `nomads` DEFAULT CHARACTER SET utf8mb4;
USE `nomads` ;

-- -----------------------------------------------------
-- Table `nomads`.`type_partenaire`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nomads`.`type_partenaire` (
  `id_type_partenaire` INT NOT NULL AUTO_INCREMENT,
  `libelle_type_partenaire` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_type_partenaire`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nomads`.`ville`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nomads`.`ville` (
  `id_ville` INT NOT NULL AUTO_INCREMENT,
  `libelle_ville` VARCHAR(255) NOT NULL,
  `code_postal` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_ville`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`partenaire`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nomads`.`partenaire` (
  `id_partenaire` INT NOT NULL AUTO_INCREMENT,
  `denomination_sociale` VARCHAR(255) NOT NULL,
  `adresse_partenaire` VARCHAR(255) NOT NULL,
  `debut_partenariat` DATE NOT NULL,
  `fin_partenariat` DATE NULL,
  `nom_contact` VARCHAR(255) NOT NULL,
  `numero_contact` VARCHAR(255) NULL,
  `mail_contact` VARCHAR(255) NOT NULL,
  `nom_secondaire` VARCHAR(255) NULL,
  `mail_secondaire` VARCHAR(255) NULL,
  `date_inscription` DATE NOT NULL,
  `date_sortie` DATE NULL,
  `id_ville` INT NOT NULL,
  `id_type_partenaire` INT NOT NULL,
  PRIMARY KEY (`id_partenaire`),
  CONSTRAINT `FK_PARTENAIRE_VILLE`
    FOREIGN KEY (`id_ville`)
    REFERENCES `nomads`.`ville` (`id_ville`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_PARTENAIRE_TYPEPART`
    FOREIGN KEY (`id_type_partenaire`)
    REFERENCES `nomads`.`type_partenaire` (`id_type_partenaire`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FK_PARTENAIRE_VILLE_idx` ON `nomads`.`partenaire` (`id_ville` ASC) VISIBLE;

CREATE INDEX `FK_PARTENAIRE_TYPEPART_idx` ON `nomads`.`partenaire` (`id_type_partenaire` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`type_recurrence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nomads`.`type_recurrence` (
  `id_type_recurrence` INT NOT NULL AUTO_INCREMENT,
  `libelle_recurrence` VARCHAR(255) NOT NULL,
  `par_pas_recurrence` INT NOT NULL,
  `duree` INT NOT NULL,
  PRIMARY KEY (`id_type_recurrence`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nomads`.`type_prestation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `nomads`.`type_prestation` (
  `id_type_prestation` INT NOT NULL AUTO_INCREMENT,
  `libelle_type_prestation` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_type_prestation`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nomads`.`type_annulation`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`type_annulation` (
  `id_type_annulation` INT NOT NULL AUTO_INCREMENT,
  `raison_annulation` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_type_annulation`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nomads`.`prestation`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`prestation` (
  `id_prestation` INT NOT NULL AUTO_INCREMENT,
  `libelle_prestation` VARCHAR(255) NOT NULL,
  `date` DATE NOT NULL,
  `duree` TIME NOT NULL,
  `masseur_min` INT NOT NULL,
  `masseur_max` INT NULL,
  `cout_mads` INT NULL,
  `nb_masseurs_inscrit` INT NULL,
  `date_creation` DATE NOT NULL,
  `date_annulation` DATE NULL,
  `adresse_prestation` VARCHAR(255) NULL,
  `libelle_fb_part` VARCHAR(255) NULL,
  `date_fb_part` DATE NULL,
  `montant_devis` DOUBLE NULL,
  `date_devis` DATE NULL,
  `montant_facture` DOUBLE NULL,
  `note_feedback` INT NULL,
  `id_partenaire` INT NOT NULL,
  `id_type_recurrence` INT NULL,
  `id_type_prestation` INT NOT NULL,
  `id_type_annulation` INT NULL,
  `id_ville` INT NULL,
  PRIMARY KEY (`id_prestation`),
  CONSTRAINT `FK_PRESTA_PARTENAIRE`
    FOREIGN KEY (`id_partenaire`)
    REFERENCES `nomads`.`partenaire` (`id_partenaire`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_PRESTA_RECURRENCE`
    FOREIGN KEY (`id_type_recurrence`)
    REFERENCES `nomads`.`type_recurrence` (`id_type_recurrence`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_PRESTA_TYPEPRESTA`
    FOREIGN KEY (`id_type_prestation`)
    REFERENCES `nomads`.`type_prestation` (`id_type_prestation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_PRESTA_ANNULATION`
    FOREIGN KEY (`id_type_annulation`)
    REFERENCES `nomads`.`type_annulation` (`id_type_annulation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_PRESTA_VILLE`
    FOREIGN KEY (`id_ville`)
    REFERENCES `nomads`.`ville` (`id_ville`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `FK_PRESTA_PARTENAIRE_idx` ON `nomads`.`prestation` (`id_partenaire` ASC) VISIBLE;

CREATE INDEX `FK_PRESTA_RECURRENCE_idx` ON `nomads`.`prestation` (`id_type_recurrence` ASC) VISIBLE;

CREATE INDEX `FK_PRESTA_TYPEPRESTA_idx` ON `nomads`.`prestation` (`id_type_prestation` ASC) VISIBLE;

CREATE INDEX `FK_PRESTA_ANNULATION_idx` ON `nomads`.`prestation` (`id_type_annulation` ASC) VISIBLE;

CREATE INDEX `FK_PRESTA_VILLE_idx` ON `nomads`.`prestation` (`id_ville` ASC) VISIBLE;

-- -----------------------------------------------------
-- Table `nomads`.`etat_materiel`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`etat_materiel` (
  `id_etat_materiel` INT NOT NULL AUTO_INCREMENT,
  `libelle_etat_materiel` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_etat_materiel`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`type_materiel`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`type_materiel` (
  `id_type_materiel` INT NOT NULL AUTO_INCREMENT,
  `libelle_type_materiel` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_type_materiel`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`materiel`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`materiel` (
  `id_materiel` INT NOT NULL AUTO_INCREMENT,
  `libelle_materiel` VARCHAR(255) NOT NULL,
  `date_achat` DATE NOT NULL,
  `id_type_materiel` INT NOT NULL,
  PRIMARY KEY (`id_materiel`),
  CONSTRAINT `FK_MATERIEL_TYPEMATER`
    FOREIGN KEY (`id_type_materiel`)
    REFERENCES `nomads`.`type_materiel` (`id_type_materiel`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_MATERIEL_TYPEMATER_idx` ON `nomads`.`materiel` (`id_type_materiel` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`type_penalite`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`type_penalite` (
  `id_type_penalite` INT NOT NULL AUTO_INCREMENT,
  `libelle_type_penalite` VARCHAR(255) NOT NULL,
  `penalite_mads` INT NOT NULL,
  `penalite_autre` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_type_penalite`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`sexe`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`sexe` (
  `id_sexe` INT NOT NULL AUTO_INCREMENT,
  `libelle_sexe` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_sexe`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`masseur`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`masseur` (
  `id_masseur` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(255) NOT NULL,
  `prenom` VARCHAR(255) NOT NULL,
  `numero_telephone` VARCHAR(255) NOT NULL,
  `date_naissance` DATE NOT NULL,
  `adresse_masseur` VARCHAR(255) NOT NULL,
  `date_inscription` DATE NOT NULL,
  `date_desinscription` DATE NULL DEFAULT NULL,
  `nombre_mads` int DEFAULT '0',
  `actif_masseur` tinyint DEFAULT '1',
  `id_sexe` INT NOT NULL,
  `id_ville` INT NOT NULL,
  PRIMARY KEY (`id_masseur`),
  CONSTRAINT `FK_MASSEUR_SEXE`
    FOREIGN KEY (`id_sexe`)
    REFERENCES `nomads`.`sexe` (`id_sexe`),
  CONSTRAINT `FK_MASSEUR_VILLE`
    FOREIGN KEY (`id_ville`)
    REFERENCES `nomads`.`ville` (`id_ville`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_MASSEUR_SEXE_idx` ON `nomads`.`masseur` (`id_sexe` ASC) VISIBLE;

CREATE INDEX `FK_MASSEUR_VILLE_idx` ON `nomads`.`masseur` (`id_ville` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`inscription_prestation`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`inscription_prestation` (
  `id_inscription_prestation` INT NOT NULL AUTO_INCREMENT,
  `date_inscription` DATE NOT NULL,
  `date_validation` DATE NULL DEFAULT NULL,
  `date_desistement` DATE NULL DEFAULT NULL,
  `cout_mads` INT NOT NULL,
  `libelle_fb_masseur` VARCHAR(255) NULL DEFAULT NULL,
  `date_fb_masseur` DATE NULL DEFAULT NULL,
  `montant_presta_intervention` DOUBLE NULL DEFAULT NULL,
  `date_facture` DATE NULL DEFAULT NULL,
  `note_feedback` INT NULL DEFAULT NULL,
  `nombre_clients` INT NULL DEFAULT NULL,
  `chiffre_affaires` DOUBLE NULL DEFAULT NULL,
  `date_reservation` DATE NULL DEFAULT NULL,
  `date_retrait` DATE NULL DEFAULT NULL,
  `date_retour` DATE NULL DEFAULT NULL,
  `cout_mads_arrhes` INT NULL DEFAULT NULL,
  `id_type_penalite` INT NULL,
  `id_materiel` INT NULL,
  `id_masseur` INT NOT NULL,
  `id_prestation` INT NOT NULL,
    PRIMARY KEY (`id_inscription_prestation`),
  CONSTRAINT `FK_INSCRIPRESTA_MATERIEL`
    FOREIGN KEY (`id_materiel`)
    REFERENCES `nomads`.`materiel` (`id_materiel`),
  CONSTRAINT `FK_INSCRIPRESTA_PENALITE`
    FOREIGN KEY (`id_type_penalite`)
    REFERENCES `nomads`.`type_penalite` (`id_type_penalite`),
  CONSTRAINT `FK_INSCRIPRESTA_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_INSCRIPRESTA_PRESTA`
    FOREIGN KEY (`id_prestation`)
    REFERENCES `nomads`.`prestation` (`id_prestation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_INSCRIPRESTA_PENALITE_idx` ON `nomads`.`inscription_prestation` (`id_type_penalite` ASC) VISIBLE;

CREATE INDEX `FK_INSCRIPRESTA_MATERIEL_idx` ON `nomads`.`inscription_prestation` (`id_materiel` ASC) VISIBLE;

CREATE INDEX `FK_INSCRIPRESTA_MASSEUR_idx` ON `nomads`.`inscription_prestation` (`id_masseur` ASC) VISIBLE;

CREATE INDEX `FK_INSRIPRESTA_PRESTA_idx` ON `nomads`.`inscription_prestation` (`id_prestation` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`jour_type`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`jour_type` (
  `id_jour_type` INT NOT NULL AUTO_INCREMENT,
  `libelle_jour_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_jour_type`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`periode_indispo`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`periode_indispo` (
  `id_periode_indispo` INT NOT NULL AUTO_INCREMENT,
  `date_debut_indispo` DATE NOT NULL,
  `date_fin_indispo` DATE NULL DEFAULT NULL,
  `id_masseur` INT NOT NULL,
  PRIMARY KEY (`id_periode_indispo`),
  CONSTRAINT `FK_PERIODEIND_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_PERIODEIND_MASSEUR_idx` ON `nomads`.`periode_indispo` (`id_masseur` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`preference_travail`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`preference_travail` (
  `id_pref_travail` INT NOT NULL AUTO_INCREMENT,
  `debut_validite` DATE NOT NULL,
  `fin_validite` DATE NOT NULL,
  `heure_debut` TIME NULL DEFAULT NULL,
  `heure_fin` TIME NULL DEFAULT NULL,
  `id_jour_type` INT NOT NULL,
  `id_masseur` INT NOT NULL,
  PRIMARY KEY (`id_pref_travail`),
  CONSTRAINT `FK_PREFTRAVAIL_JOURTYPE`
    FOREIGN KEY (`id_jour_type`)
    REFERENCES `nomads`.`jour_type` (`id_jour_type`),
  CONSTRAINT `FK_PREFTRAVAIL_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_PREFTRAVAIL_JOURTYPE_idx` ON `nomads`.`preference_travail` (`id_jour_type` ASC) VISIBLE;

CREATE INDEX `FK_PREFTRAVAIL_MASSEUR_idx` ON `nomads`.`preference_travail` (`id_masseur` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`ref_competence`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`ref_competence` (
  `id_ref_competence` INT NOT NULL AUTO_INCREMENT,
  `libelle_competence` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_ref_competence`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `nomads`.`suivi_competence`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`suivi_competence` (
  `id_suivi_competence` INT NOT NULL AUTO_INCREMENT,
  `date_debut` DATE NULL DEFAULT NULL,
  `date_fin` DATE NULL DEFAULT NULL,
  `id_ref_competence` INT NULL DEFAULT NULL,
  `id_masseur` INT NOT NULL,
  PRIMARY KEY (`id_suivi_competence`),
  CONSTRAINT `FK_SUIVICOMP_REFCOMP`
    FOREIGN KEY (`id_ref_competence`)
    REFERENCES `nomads`.`ref_competence` (`id_ref_competence`),
  CONSTRAINT `FK_SUIVICOMP_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_SUIVICOMP_REFCOMP_idx` ON `nomads`.`suivi_competence` (`id_ref_competence` ASC) VISIBLE;

CREATE INDEX `FK_SUIVICOMP_MASSEUR_idx` ON `nomads`.`suivi_competence` (`id_masseur` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`suivi_etat`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`suivi_etat` (
  `date_verification` DATE NOT NULL,
  `id_materiel` INT NOT NULL,
  `id_etat_materiel` INT NOT NULL,
  PRIMARY KEY (`id_materiel`, `id_etat_materiel`),
  CONSTRAINT `FK_SUIVIETAT_ETATMATERIEL`
    FOREIGN KEY (`id_etat_materiel`)
    REFERENCES `nomads`.`etat_materiel` (`id_etat_materiel`),
  CONSTRAINT `FK_SUIVIETAT_MATERIEL`
    FOREIGN KEY (`id_materiel`)
    REFERENCES `nomads`.`materiel` (`id_materiel`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_SUIVIETAT_MATERIEL_idx` ON `nomads`.`suivi_etat` (`id_materiel` ASC) VISIBLE;

CREATE INDEX `FK_SUIVIETAT_ETATMATERIEL_idx` ON `nomads`.`suivi_etat` (`id_etat_materiel` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`transaction`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`transaction` (
  `id_transaction` INT NOT NULL AUTO_INCREMENT,
  `quantite_pack` INT NOT NULL,
  `date_achat` DATE NOT NULL,
  `id_masseur` INT NOT NULL,
  PRIMARY KEY (`id_transaction`),
  CONSTRAINT `FK_TRANSACTION_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_TRANSACTION_MASSEUR_idx` ON `nomads`.`transaction` (`id_masseur` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`type_de_pack`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`type_de_pack` (
  `id_type_de_pack` INT NOT NULL AUTO_INCREMENT,
  `libelle_type_de_pack` VARCHAR(255) NOT NULL,
  `quantite_mads` INT NULL DEFAULT NULL,
  `prix` INT NULL DEFAULT NULL,
  `id_transaction` INT NOT NULL,
  PRIMARY KEY (`id_type_de_pack`),
  CONSTRAINT `FK_TYPEPACK_TRANSACTION`
    FOREIGN KEY (`id_transaction`)
    REFERENCES `nomads`.`transaction` (`id_transaction`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_TYPEPACK_TRANSACTION_idx` ON `nomads`.`type_de_pack` (`id_transaction` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`ville_intervention`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`ville_intervention` (
  `id_masseur` INT NOT NULL,
  `id_ville` INT NOT NULL,
  `date_debut_choix` DATE NOT NULL,
  `date_fin_choix` DATE NULL,
  PRIMARY KEY (`id_masseur`, `id_ville`),
  CONSTRAINT `FK_VILLEINTERV_MASSEUR`
    FOREIGN KEY (`id_masseur`)
    REFERENCES `nomads`.`masseur` (`id_masseur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_VILLEINTERV_VILLE`
    FOREIGN KEY (`id_ville`)
    REFERENCES `nomads`.`ville` (`id_ville`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_VILLEINTERV_VILLE_idx` ON `nomads`.`ville_intervention` (`id_ville` ASC) VISIBLE;

CREATE INDEX `FK_VILLEINTERV_MASSEUR_idx` ON `nomads`.`ville_intervention` (`id_masseur` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `nomads`.`besoin_materiel`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE IF NOT EXISTS `nomads`.`besoin_materiel` (
  `id_type_materiel` INT NOT NULL,
  `id_prestation` INT NOT NULL,
  `quantite` INT NOT NULL,
  PRIMARY KEY (`id_type_materiel`, `id_prestation`),
  CONSTRAINT `FK_BESOINMATER_TYPEMATER`
    FOREIGN KEY (`id_type_materiel`)
    REFERENCES `nomads`.`type_materiel` (`id_type_materiel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_BESOINMATER_PRESTA`
    FOREIGN KEY (`id_prestation`)
    REFERENCES `nomads`.`prestation` (`id_prestation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE INDEX `FK_BESOINMATER_PRESTA_idx` ON `nomads`.`besoin_materiel` (`id_prestation` ASC) VISIBLE;

CREATE INDEX `FK_BESOINMATER_TYPEMATER_idx` ON `nomads`.`besoin_materiel` (`id_type_materiel` ASC) VISIBLE;

-- -----------------------------------------------------
-- Table `nomads`.`emprunt_materiel`
-- -----------------------------------------------------
/*!50503 SET character_set_client = utf8mb4 */;

CREATE TABLE `nomads`.`emprunt_materiel` (
  `id_materiel` INT NOT NULL,
  `id_inscription_prestation` INT NOT NULL,
  PRIMARY KEY (`id_materiel`, `id_inscription_prestation`),
  INDEX `FK_EMPRUNTM_INSCRIPRESTA_idx` (`id_inscription_prestation` ASC) VISIBLE,
  CONSTRAINT `FK_EMPRUNTM_MATERIEL`
    FOREIGN KEY (`id_materiel`)
    REFERENCES `nomads`.`materiel` (`id_materiel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_EMPRUNTM_INSCRIPRESTA`
    FOREIGN KEY (`id_inscription_prestation`)
    REFERENCES `nomads`.`inscription_prestation` (`id_inscription_prestation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;