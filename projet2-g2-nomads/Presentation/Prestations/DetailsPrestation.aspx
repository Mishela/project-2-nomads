﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="DetailsPrestation.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Prestations.DetailsPrestation" %>

<%@ Register Src="~/Prestations/UCToggleBarPrestation.ascx" TagPrefix="uc1" TagName="UCToggleBarPrestation" %>
<%@ Register Src="~/Masseurs/UCRechecheMasseurs.ascx" TagPrefix="uc1" TagName="UCRechecheMasseurs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPrestation runat="server" ID="UCToggleBarPrestation" />
    <!--  Pour factoriser la recherche il faudrait mettre un controleur ici à la place de la nav  -->
    <nav>
         Nom du Masseur : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>         
        Prénom:<asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox>
        Compétence : <asp:DropDownList ID="ddlComp" runat="server"></asp:DropDownList>
        sexe : <asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList>
        Ville : <asp:DropDownList   ID="ddlVille" runat="server"></asp:DropDownList> 
         
        
       <asp:Button ID="btnSearchMasseurs" runat="server" OnClick="btnSearchMasseurs_Click" Text="Chercher des masseurs compatibles" />
    </nav>

    <div class="grid-liste">
        
         <!--Table Détails Prestation-->
         <div class=" masseur liste-item">
             <h2><asp:Label ID="lblLibelle" runat="server"></asp:Label></h2>
             <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>

             <table>
                    <tr>
                        <td>Date :</td>
                        <td><asp:Label ID="lblDate" runat="server" ></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Durée :</td>
                        <td><asp:Label ID="lblDuree" runat="server" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Nombre de masseurs minimum :</td>
                        <td><asp:Label ID="lblMasseurMin" runat="server" ></asp:Label></td>
                    </tr>
                  <tr>
                        <td>Nombre de masseurs maximum :</td>
                        <td><asp:Label ID="lblMasseurMax" runat="server" ></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Adresse :</td>
                        <td><asp:Label ID="lblAdresse" runat="server"></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Mads nécessaires pour s'inscrire à la prestation :</td>
                        <td><h3><asp:Label ID="lblMads" runat="server" ></asp:Label></h3></td>
                    </tr>
                     
                    <tr>
                <td class="FirstCol"></td>
                <td class="SecondCol"><asp:Button ID="btnModifier" runat="server" Text="Modifier" OnClick="btnModifier_Click" /></td>
            </tr>

                </table>  
         </div>


        <!-- Recherche masseurs -->
        

        <!--GridView Masseurs Inscrits-->
        <div class="masseur liste-item">
            <h1><asp:Label ID="Label1" runat="server"></asp:Label>Masseurs Inscrits</h1>

                <asp:GridView  ID="gvMasseursInscrits" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="false" OnRowCommand="gvMasseursInscrits_RowCommand">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                
                    <Columns>
                        <asp:BoundField DataField="Nom" HeaderText="Nom" />
                        <asp:BoundField DataField="Prenom" HeaderText="Prénom" />
                        <asp:BoundField DataField="Inscription" HeaderText="Date d'Inscription" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDesinscriptionMasseur" runat="server" 
                                    CommandName="btnDesinscriptionPrestations_OnClick" 
                                    CommandArgument='<%# Bind("IdMasseur")%>' 
                                    Text="Désinscription" />
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        

        

    </div>
</asp:Content>
