﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class ListePrestations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrestationBU bu = new PrestationBU();
               // List<PrestationEntity> tournees = new List<PrestationEntity>();

                TypePrestationBU typeBU = new TypePrestationBU();
                List<TypePrestationEntity> types = typeBU.GetListTypePrestation();
                types.Insert(0, new TypePrestationEntity(-1, "Type prestation"));

                // DATABINDING : 

                ddlType.DataValueField = "IdTypePrestation"; 
                ddlType.DataTextField = "LibellePrestation";
                ddlType.DataSource = types;
                ddlType.DataBind();

                rptPrestations.DataSource = bu.GetListPrestations();
                rptPrestations.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PrestationBU bu = new PrestationBU();
            string nom = txtNom.Text;
            int type = int.Parse(ddlType.SelectedValue);

            //DateTime? date = null;
            //if (! DateTime.IsNullOrEmpty()
            //{

            //}

            rptPrestations.DataSource = bu.RechercherPresta(nom, type);
            
            rptPrestations.DataBind();

        }
        
    }
}