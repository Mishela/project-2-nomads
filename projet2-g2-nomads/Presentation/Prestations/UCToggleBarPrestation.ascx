﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCToggleBarPrestation.ascx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Tournees.UCToggleBarPrestation" %>
<nav class="underNav">
    <ul>
        <li><a href="/AccueilNomads.aspx">Accueil</a></li>
        <li><a href="/Prestations/ListePrestations.aspx">Liste des prestations</a></li>
        <li><a href="/Masseurs/ListeMasseurs.aspx">Liste des Masseurs</a></li>
        <li><a href="AjoutPrestation.aspx">Ajouter une nouvelle prestation</a></li>
    </ul>
</nav>
