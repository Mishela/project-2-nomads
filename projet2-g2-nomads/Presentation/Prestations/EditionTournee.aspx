﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionTournee.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.EditionTournee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Edition Masseur</h1>

    <table>
 <tr>
    <td class="label">Nom de la tournée :</td>
    <td><asp:TextBox ID="txtLibelle" runat="server" Width="225px"></asp:TextBox> </td>
 </tr>

 <tr>
    <!--  <td class="label">Date de la tournée : </td>
     <td><asp:TextBox ID="txtDate" runat="server" Width="225px"></asp:TextBox> </td> -->
     <td class="label">Durée de la tournée :  </td>
     <td><asp:TextBox ID="dateDebut" TextMode="Date" runat="server" OnTextChanged="dateDebut_TextChanged" Width="125px"></asp:TextBox></td>
  </tr>

     <tr>
    <td class="label">Durée de la tournée :  </td>
     <td><asp:DropDownList ID="ddlDuree" runat="server"> </asp:DropDownList></td> 
  </tr>

  <tr>
     <td class="label">Nombre de masseurs au minimum requis :  </td>
     <td><asp:TextBox ID="txtNbMasseurMin" runat="server" Width="225px"></asp:TextBox> </td>
  </tr>

  <tr>
     <td class="label">Coût Mads pour la tournée :  </td>
     <td><asp:TextBox ID="txtCoutMads" runat="server" Width="225px"></asp:TextBox> </td>
  </tr>
  <tr>
      <td class="label">Adresse de la tournée :  </td>
      <td><asp:TextBox ID="txtAddresse" runat="server" TextMode="MultiLine" Width="225px"></asp:TextBox></td>
  </tr>
   <tr> 
      <td class="label">Ville :  </td>
      <td> <asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList>
  </tr>
  <tr> 
      <td class="label">Type Prestation :  </td>
      <td> <asp:DropDownList ID="ddlTypePrestation" runat="server"></asp:DropDownList>
  </tr>
  <tr> 
      <td class="label">Partenaire :  </td>
      <td> <asp:DropDownList ID="ddlPartenaire" runat="server"></asp:DropDownList>
  </tr>
    
   <tr>
      <td></td>   
      <td>
          <br />
          <br />
          <asp:Button ID="btnValider" runat="server" Text="Valider" OnClick="btnValider_Click"/>
          <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click"/>
          
     </td>
  </tr>
 </table>

</asp:Content>
