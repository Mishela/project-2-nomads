﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class AjoutPrestation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)

            {
                PartenaireBU buPart= new PartenaireBU();
                List<PartenaireEntity> partenaire = buPart.GetListePartenaires();
                ddlPartenaire.DataTextField = "DenominationSociale";
                ddlPartenaire.DataValueField = "IdPartenaire";
                ddlPartenaire.DataSource = partenaire;
                ddlPartenaire.DataBind();

                ddlDuree.Items.Add("01:00:00");
                ddlDuree.Items.Add("01:30:00");
                ddlDuree.Items.Add("02:00:00");
                ddlDuree.Items.Add("02:30:00");
                ddlDuree.Items.Add("03:00:00");
                ddlDuree.Items.Add("03:30:00");
                ddlDuree.Items.Add("04:00:00");
                ddlDuree.Items.Add("04:30:00");
                ddlDuree.Items.Add("05:00:00");
                ddlDuree.Items.Add("05:30:00");
                ddlDuree.Items.Add("06:00:00");
                ddlDuree.Items.Add("06:30:00");
                ddlDuree.Items.Add("07:00:00");
                ddlDuree.Items.Add("07:30:00");
                ddlDuree.Items.Add("08:00:00");
                ddlDuree.Items.Add("08:30:00");

                TypePrestationBU buTP = new TypePrestationBU();
                ddlTypePrestation.DataTextField = "LibellePrestation";
                ddlTypePrestation.DataValueField = "IdTypePrestation";
                ddlTypePrestation.DataSource = buTP.GetListTypePrestation();
                ddlTypePrestation.DataBind();

                VilleBU buVille = new VilleBU();
                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = buVille.GetListVille();
                ddlVille.DataBind();

                JeuxDonneesAjoutPresta donneesDemo = new JeuxDonneesAjoutPresta();
                PrestationEntity prestaChampVide = new PrestationEntity();
                PrestationEntity prestaDateInvalide = new PrestationEntity();
                PrestationEntity prestaDemoValide = new PrestationEntity();
                prestaChampVide = donneesDemo.DemoChampVide();
                prestaDateInvalide = donneesDemo.DemoDateInvalide();
                prestaDemoValide = donneesDemo.DemoPrestaNbMasseurNonValide();
                ddlDemoAjoutPresta.Items.Add(prestaChampVide.Libelle);
                ddlDemoAjoutPresta.Items.Add(prestaDateInvalide.Libelle);
                ddlDemoAjoutPresta.Items.Add(prestaDemoValide.Libelle);


            }


        }
        
        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Prestations/ListePrestations.aspx");
        }


        protected void btnValider_Click(object sender, EventArgs e)
        {
            PrestationEntity nouvelleTournee = new PrestationEntity();
            
            nouvelleTournee.Libelle = txtLibelle.Text;
            DateTime dateTournee;
            DateTime.TryParse(dateDebut.Text, out dateTournee);
            nouvelleTournee.Date = dateTournee;
            nouvelleTournee.Duree = ddlDuree.Text.ToString();
            
                int masseurMin;
                int.TryParse(txtNbMasseurMin.Text, out masseurMin);
                nouvelleTournee.MasseurMin = masseurMin; //int.Parse(txtNbMasseurMin.Text);
                int coutMads;
                int.TryParse(txtCoutMads.Text, out coutMads);
                nouvelleTournee.CoutMads = coutMads; // int.Parse(txtCoutMads.Text);
            nouvelleTournee.Adresse = txtAddresse.Text.ToString();
            nouvelleTournee.IdPartenaire = int.Parse(ddlPartenaire.SelectedValue);
            nouvelleTournee.DateCreation = DateTime.Today;
            nouvelleTournee.IdTypePrestation = int.Parse(ddlTypePrestation.SelectedValue);
            nouvelleTournee.IdVille = int.Parse(ddlVille.SelectedValue);
            nouvelleTournee.MasseurMax = int.Parse(txtNbMasseurMax.Text);

          


            try
            {
                PrestationBU bu = new PrestationBU();
                bu.AjouterTournee(nouvelleTournee);
                lblMessage.Text = "La prestation a bien été ajoutée!";
                lblMessage.Visible = true;
               
            }
            catch(Exception exp)
            {
                lblMessage.Text = exp.Message;
                lblMessage.Visible = true;
                
                
            }
            
        }
        protected void btnDemo_Click(object sender, EventArgs e)
        {
            JeuxDonneesAjoutPresta donneesDemo = new JeuxDonneesAjoutPresta();
            PrestationEntity prestaChampVide = new PrestationEntity();
            PrestationEntity prestaDateInvalide = new PrestationEntity();
            PrestationEntity demoPrestaNbMasseurNonValide = new PrestationEntity();
            prestaChampVide = donneesDemo.DemoChampVide();
            prestaDateInvalide = donneesDemo.DemoDateInvalide();
            demoPrestaNbMasseurNonValide = donneesDemo.DemoPrestaNbMasseurNonValide();
            string prestaChoisi = ddlDemoAjoutPresta.Text;
            if(prestaChoisi == prestaChampVide.Libelle)
            {

                txtLibelle.Text = prestaChampVide.Libelle;
                dateDebut.Text = prestaChampVide.Date.ToString("yyyy-MM-dd");
                ddlDuree.Text = prestaChampVide.Duree;
               /* txtNbMasseurMin.Text = prestaChampVide.MasseurMin.ToString();
                txtCoutMads.Text = prestaChampVide.CoutMads.ToString();*/
                txtAddresse.Text = prestaChampVide.Adresse;
                ddlPartenaire.SelectedValue = prestaChampVide.IdPartenaire.ToString();
                ddlTypePrestation.SelectedValue = prestaChampVide.IdTypePrestation.ToString();
                ddlVille.SelectedValue = prestaChampVide.IdVille.ToString();
                txtNbMasseurMax.Text=prestaChampVide.MasseurMax.ToString();

            }
            if (prestaChoisi == prestaDateInvalide.Libelle)
            {
               
                

                txtLibelle.Text = prestaDateInvalide.Libelle;
                dateDebut.Text = prestaDateInvalide.Date.ToString("yyyy-MM-dd");
                ddlDuree.Text = prestaDateInvalide.Duree;
                txtNbMasseurMin.Text = prestaDateInvalide.MasseurMin.ToString();
                txtCoutMads.Text = prestaDateInvalide.CoutMads.ToString();
                txtAddresse.Text = prestaDateInvalide.Adresse;
                ddlPartenaire.SelectedValue = prestaDateInvalide.IdPartenaire.ToString();
                ddlTypePrestation.SelectedValue = prestaDateInvalide.IdTypePrestation.ToString();
                ddlVille.SelectedValue = prestaDateInvalide.IdVille.ToString();
                txtNbMasseurMax.Text = prestaDateInvalide.MasseurMax.ToString();
            }
            if (prestaChoisi == demoPrestaNbMasseurNonValide.Libelle)
            {
                txtLibelle.Text = demoPrestaNbMasseurNonValide.Libelle;
                dateDebut.Text = demoPrestaNbMasseurNonValide.Date.ToString("yyyy-MM-dd");
                ddlDuree.Text = demoPrestaNbMasseurNonValide.Duree;
                txtNbMasseurMin.Text = demoPrestaNbMasseurNonValide.MasseurMin.ToString();
                txtCoutMads.Text = demoPrestaNbMasseurNonValide.CoutMads.ToString();
                txtAddresse.Text = demoPrestaNbMasseurNonValide.Adresse;
                ddlPartenaire.SelectedValue = demoPrestaNbMasseurNonValide.IdPartenaire.ToString();
                ddlTypePrestation.SelectedValue = demoPrestaNbMasseurNonValide.IdTypePrestation.ToString();
                ddlVille.SelectedValue = demoPrestaNbMasseurNonValide.IdVille.ToString();
                txtNbMasseurMax.Text = demoPrestaNbMasseurNonValide.MasseurMax.ToString();
            }
        }
        
    }
}