﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class ModifierPrestation : System.Web.UI.Page
    {
        private int idPrestation;
        protected void Page_Load(object sender, EventArgs e)
        {
           

            this.idPrestation = int.Parse(Request["id"]);

            if (!IsPostBack)
            {
               
                PartenaireBU buPart = new PartenaireBU();
                List<PartenaireEntity> partenaire = buPart.GetListePartenaires();
              
                ddlPartenaire.DataTextField = "DenominationSociale";
                ddlPartenaire.DataValueField = "IdPartenaire";
                ddlPartenaire.DataSource = partenaire;
                ddlPartenaire.DataBind();

                ddlDuree.Items.Add("01:00:00");
                ddlDuree.Items.Add("01:30:00");
                ddlDuree.Items.Add("02:00:00");
                ddlDuree.Items.Add("02:30:00");
                ddlDuree.Items.Add("03:00:00");
                ddlDuree.Items.Add("03:30:00");
                ddlDuree.Items.Add("04:00:00");
                ddlDuree.Items.Add("04:30:00");
                ddlDuree.Items.Add("05:00:00");
                ddlDuree.Items.Add("05:30:00");
                ddlDuree.Items.Add("06:00:00");
                ddlDuree.Items.Add("06:30:00");
                ddlDuree.Items.Add("07:00:00");
                ddlDuree.Items.Add("07:30:00");
                ddlDuree.Items.Add("08:00:00");
                ddlDuree.Items.Add("08:30:00");

                TypePrestationBU buTP = new TypePrestationBU();
                ddlTypePrestation.DataTextField = "LibellePrestation";
                ddlTypePrestation.DataValueField = "IdTypePrestation";
                ddlTypePrestation.DataSource = buTP.GetListTypePrestation();
                ddlTypePrestation.DataBind();

                VilleBU buVille = new VilleBU();
                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = buVille.GetListVille();
                ddlVille.DataBind();

                PrestationBU bu = new PrestationBU();
                PrestationEntity tournee = bu.GetPrestationById(this.idPrestation);

                txtLibelle.Text = tournee.Libelle;
                dateDebut.Text = tournee.Date.ToString("yyyy-MM-dd");
                ddlDuree.SelectedValue = tournee.Duree;
                txtNbMasseurMin.Text = tournee.MasseurMin.ToString();
                txtNbMasseurMax.Text = tournee.MasseurMax.ToString();
                txtCoutMads.Text = tournee.CoutMads.ToString();
                txtAddresse.Text = tournee.Adresse;
                ddlVille.SelectedValue = tournee.IdVille.ToString();
                ddlTypePrestation.SelectedValue = tournee.IdTypePrestation.ToString();
                ddlPartenaire.SelectedValue = tournee.IdPartenaire.ToString();

               /* if(tournee.Date < DateTime.Now)
                {
                   MessageBox.Show("La date de la tournée ne peut être inférieur à la date actuelle");
                }
                if(tournee.Date == DateTime.Now)
                {
                    MessageBox.Show("La date de la tournée doit être supérieur à la date actuelle");
                }
                */
            }
        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Prestations/DetailsPrestation.aspx?idprestation="+this.idPrestation.ToString());
        }
     
        protected void btnValider_Click(object sender, EventArgs e)
        {
            PrestationEntity tourneeModifiee = new PrestationEntity();
            try
            {
                tourneeModifiee.IdPrestation = idPrestation;
                tourneeModifiee.Libelle = txtLibelle.Text;
                DateTime dateTournee;
                DateTime.TryParse(dateDebut.Text, out dateTournee);
                tourneeModifiee.Date = dateTournee;
                tourneeModifiee.Duree = ddlDuree.Text;
                tourneeModifiee.MasseurMin = int.Parse(txtNbMasseurMin.Text);
                tourneeModifiee.MasseurMax = int.Parse(txtNbMasseurMax.Text);
                tourneeModifiee.CoutMads = int.Parse(txtCoutMads.Text);
                tourneeModifiee.Adresse = txtAddresse.Text;
                tourneeModifiee.IdPartenaire = int.Parse(ddlPartenaire.SelectedValue);
                tourneeModifiee.DateCreation = DateTime.Today;
                tourneeModifiee.IdTypePrestation = int.Parse(ddlTypePrestation.SelectedValue);
                tourneeModifiee.IdVille = int.Parse(ddlVille.SelectedValue);
            }catch (Exception exp)
            {
                lblMessage.Text = "Veuillez remplir tous les champs";
                lblMessage.Visible = true;
            }

            try
            {
                PrestationBU bu = new PrestationBU();
                bu.ModifierTournee(tourneeModifiee);
                lblMessage.Text = "La prestation a bien été modifié!";
                

            }catch (Exception exp)
            {
                lblMessage.Text = exp.Message;
                lblMessage.Visible = true;
            }
             

            
        }
    }
}