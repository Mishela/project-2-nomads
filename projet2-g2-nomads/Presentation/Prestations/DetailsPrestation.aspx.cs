﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Prestations
{
    public partial class DetailsPrestation : System.Web.UI.Page
    {
       PrestationBU prestationBU = new PrestationBU();
        private int idPrestation;
        private int idMasseur;

        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["idprestation"], out idPrestation);

            if (!IsPostBack)
            {
                PrestationEntity prestation = prestationBU.GetPrestationById(idPrestation);

                if (prestation != null)
                {
                    lblLibelle.Text = prestation.Libelle;
                    lblID.Text = prestation.IdPrestation.ToString();
                    lblDate.Text = prestation.Date.ToString("dd MMM yyyy");
                    lblDuree.Text = prestation.Duree.ToString();
                    lblAdresse.Text = prestation.Adresse;
                    lblMasseurMin.Text = prestation.MasseurMin.ToString();
                    lblMasseurMax.Text = prestation.MasseurMax.ToString();
                    lblMads.Text = prestation.CoutMads.ToString();


                    //lblParticularités.Text = prestation.Particularités;

                    // BINDING POUR LA RECHERCHE !
                    VilleBU VilleBU = new VilleBU();
                    List<VilleEntity> villes = VilleBU.GetListVille();
                    villes.Insert(0, new VilleEntity(-1, "ville"));

                    SexeBU SexeBU = new SexeBU();
                    List<SexeEntity> sexes = SexeBU.GetListSexe();
                    sexes.Insert(0, new SexeEntity(-1, "Sexe"));

                    CompetenceBU CompBU = new CompetenceBU();
                    List<RefCompetenceEntity> competences = CompBU.GetListCompetence();
                    competences.Insert(0, new RefCompetenceEntity(-1, "Compétences"));

                    // Databinding pour les champs du filtre =>
                    ddlVille.DataValueField = "IdVille";
                    ddlVille.DataTextField = "Nom";
                    ddlVille.DataSource = villes;
                    ddlVille.DataBind();

                    ddlComp.DataValueField = "IdCompetence";
                    ddlComp.DataTextField = "Libelle";
                    ddlComp.DataSource = competences;
                    ddlComp.DataBind();

                    ddlSexe.DataValueField = "IdSexe";
                    ddlSexe.DataTextField = "Libelle";
                    ddlSexe.DataSource = sexes;
                    ddlSexe.DataBind();
                    // FIN BINDING POUR LA RECHERCHE !

                }

                MasseursBU masseurBU = new MasseursBU();
                gvMasseursInscrits.DataSource = masseurBU.GetListMasseursByPrestation(idPrestation);
                gvMasseursInscrits.DataBind();

            }
        }

        protected void btnInscpriptionPresta_Click(object sender, EventArgs e)
        {

            InscriptionPrestationEntity nouvelleInscription = new InscriptionPrestationEntity();

            nouvelleInscription.DateInscription = DateTime.Now;
            nouvelleInscription.CoutMads = int.Parse(lblMads.Text);
            nouvelleInscription.IdPrestation = int.Parse(lblID.Text);
            nouvelleInscription.IdMasseur = int.Parse(Request["idmasseur"]);

            
            prestationBU.InscriptionPrestation(nouvelleInscription);

            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Inscription enregistrée');window.location ='/Masseurs/InscriptionPrestationMasseur.aspx?idmasseur='+ Request[\"idmasseur\"];", true);

        }

        protected void gvMasseursInscrits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnDesinscriptionPrestations_OnClick")
            {
                idMasseur = int.Parse(Request["idmasseur"]);
                prestationBU.DesinscriptionPrestation(int.Parse(e.CommandArgument.ToString()), idPrestation);
                Response.Redirect("/Prestations/DetailsPrestation.aspx?idprestation=" + idPrestation.ToString() + "&idmasseur=" + e.CommandArgument.ToString());

            }

        }

        protected void btnSearchMasseurs_Click(object sender, EventArgs e)
        {
            // Databinding pour la DTable =>

            MasseursBU MaBU = new MasseursBU();
            
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            int competence = int.Parse(ddlComp.SelectedValue);
            int sexe = int.Parse(ddlSexe.SelectedValue);
            int ville = int.Parse(ddlVille.SelectedValue);


        }
        protected void btnModifier_Click(object sender, EventArgs e)
        {
            Response.Redirect("ModifierPrestation.aspx?id=" + this.idPrestation.ToString());
        }
    }
}
