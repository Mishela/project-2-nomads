﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="ModifierPrestation.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.ModifierPrestation" %>

<%@ Register Src="~/Prestations/UCToggleBarPrestation.ascx" TagPrefix="uc1" TagName="UCToggleBarPrestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPrestation runat="server" ID="UCToggleBarPrestation" />

    <div class="masseur-item" id="masseur-prestation">
<table>
 <tr>
    <td class="label">Nom de la prestation :</td>
    <td><asp:TextBox ID="txtLibelle" runat="server" ></asp:TextBox> </td>
 </tr>

 <tr>
    
      <td class="label">Date de la prestation :  </td>
    <td><asp:TextBox ID="dateDebut" runat="server" TextMode="date"></asp:TextBox></td>
    
  </tr>

     <tr>
    <td class="label">Durée de la prestation :  </td>
     <td><asp:DropDownList ID="ddlDuree" runat="server"> </asp:DropDownList> heures</td> 
  </tr>

  <tr>
     <td class="label">Nombre de masseurs au minimum requis :  </td>
     <td><asp:TextBox ID="txtNbMasseurMin" runat="server" Width="225px"></asp:TextBox> </td>
  </tr>
      <tr>
     <td class="label">Nombre de masseurs au maximum :  </td>
     <td><asp:TextBox ID="txtNbMasseurMax" runat="server" Width="225px"></asp:TextBox> </td>
  </tr>
  <tr>
     <td class="label">Coût Mads pour la tournée :  </td>
     <td><asp:TextBox ID="txtCoutMads" runat="server" Width="225px"></asp:TextBox> </td>
  </tr>
  <tr>
      <td class="label">Adresse de la prestation :  </td>
      <td><asp:TextBox ID="txtAddresse" runat="server" TextMode="MultiLine" Width="225px"></asp:TextBox></td>
  </tr>
   <tr> 
      <td class="label">Ville :  </td>
      <td> <asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList>
  </tr>
  <tr> 
      <td class="label">Type Prestation :  </td>
      <td> <asp:DropDownList ID="ddlTypePrestation" runat="server"></asp:DropDownList>
  </tr>
  <tr> 
      <td class="label">Partenaire :  </td>
      <td> <asp:DropDownList ID="ddlPartenaire" runat="server"></asp:DropDownList>
  </tr>
    
   <tr>
      <td></td>   
      <td>
          <br />
          <br />
          <asp:Button ID="btnValider" runat="server" Text="Valider" OnClick="btnValider_Click"/>
          <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click"/>
          
     </td>
  </tr>
 </table>
    </div>
    <asp:Label ID="lblMessage" Visible="false" runat="server" Text=""></asp:Label>
</asp:Content>

