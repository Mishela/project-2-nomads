﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="ListePrestations.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.ListePrestations" %>

<%@ Register Src="~/Prestations/UCToggleBarPrestation.ascx" TagPrefix="uc1" TagName="UCToggleBarPrestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPrestation runat="server" ID="UCToggleBarPrestation" />
 <h1>Liste des Prestations</h1>

    <div class="champs">  <!-- la recherche  type / nom / Date-->
        Nom de la Prestation : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
        Type : <asp:DropDownList   ID="ddlType" runat="server"></asp:DropDownList>  
        <!--Date de la presta : <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar> -->

        <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Rechercher" />
    </div>
    <br />
    <div class="grid-liste">
        <asp:Repeater ID="rptPrestations" runat="server">
                <ItemTemplate>
                <table class="masseur liste-item">
                    <tr>
                        <td class="FirstCol"><h4><%# DataBinder.Eval(Container.DataItem, "Libelle") %></h4></td>
                        <td class="SecondCol"></td>
                        <td><a href="/Prestations/DetailsPrestation.aspx?idprestation=<%# DataBinder.Eval(Container.DataItem, "IdPrestation")%>&idmasseur=<%=Request["idmasseur"]%>">Détails</a>
                    </tr>
                    <tr>
                        <td class="FirstCol">Date :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Date", "{0:dd MMM yyyy}") %></td>
                    </tr>
                     <tr>
                        <td class="FirstCol">Adresse :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Adresse") %></td>
                    </tr> 
                    <tr>
                        <td class="FirstCol">Durée :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Duree") %></td>
                    </tr>   
                    <tr>
                        <td class="FirstCol">Coût en Mads :</td>
                        <td class="SecondCol"><strong> <%# DataBinder.Eval(Container.DataItem, "CoutMads") %></strong></td>
                    </tr>              
                  </table>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    
</asp:Content>

