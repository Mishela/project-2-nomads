﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Tournees
{
    public partial class EditionTournee : System.Web.UI.Page
    {
        private int idPrestation;
        protected void Page_Load(object sender, EventArgs e)
        {/*
            this.idPrestation = int.Parse(Request["id"]);
            if (!IsPostBack)
            {
                VilleBU villeBu = new VilleBU();
                List<VilleEntity> villes = villeBu.GetListVille();

                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = villes;
                ddlVille.DataBind();

                MasseursBU bu = new MasseursBU();
                MasseurEntity masseur = bu.GetMasseur(this.idMasseur);


                txtNom.Text = masseur.Nom;
                txtPrenom.Text = masseur.Prenom;
                txtAdress.Text = masseur.Adresse;
                ddlSexe.SelectedValue = masseur.IdSexe.ToString();
                txtTelephone.Text = masseur.NumeroTelephone;
                ddlVille.SelectedValue = masseur.IdVille.ToString();
                txtDateNaissance.Text = masseur.DateNaissance.ToString();

            }
        }

        protected void btnModifier_Click(object sender, EventArgs e)
        {
            MasseurEntity masseur = new MasseurEntity();
            masseur.IdMasseur = idMasseur;
            masseur.Nom = txtNom.Text.ToUpper();
            masseur.Prenom = toCamelCase(txtPrenom.Text);
            masseur.Adresse = txtAdress.Text;
            masseur.NumeroTelephone = txtTelephone.Text;        // Trouver une procédure de vérification du format
            masseur.IdSexe = int.Parse(ddlSexe.SelectedValue);
            masseur.IdVille = int.Parse(ddlVille.SelectedValue);
            masseur.DateNaissance = DateTime.Parse(txtDateNaissance.Text);

            MasseursBU bu = new MasseursBU();
            bu.ModifierMasseur(masseur);

            // Redirection 
            Response.Redirect("Masseur.aspx?id=" + this.idMasseur.ToString());
        }

        protected string toCamelCase(string str)
        {
            string firtsLetter = str.Substring(0, 1).ToUpper();
            string nextLetters = str.Substring(1).ToLower();
            string camelCase = firtsLetter + nextLetters;
            return camelCase.Trim();*/
        }

    
    }
}
