﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Tournees
{
    public partial class DetailsPrestationInscription : System.Web.UI.Page
    {
        PrestationBU prestationBU = new PrestationBU();
        private int idPrestation;
        private int idMasseur;

        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["idprestation"], out idPrestation);
            

            if (!IsPostBack)
            {
                PrestationEntity prestation = prestationBU.GetPrestationById(idPrestation);

                if (prestation != null)
                {
                    lblLibelle.Text = prestation.Libelle;
                    lblID.Text = prestation.IdPrestation.ToString();
                    lblDate.Text = prestation.Date.ToString("dd MMM yyyy");
                    lblDuree.Text = prestation.Duree.ToString();
                    lblAdresse.Text = prestation.Adresse;
                    lblMasseurMin.Text = prestation.MasseurMin.ToString();
                    lblMasseurMax.Text = prestation.MasseurMax.ToString();
                    lblMads.Text = prestation.CoutMads.ToString();
                 
                    //lblParticularités.Text = prestation.Particularités;
                }
                
                MasseursBU masseurBU = new MasseursBU();
                gvMasseursInscrits.DataSource = masseurBU.GetListMasseursByPrestation(idPrestation);
                gvMasseursInscrits.DataBind();

            }
        }

        protected void btnInscpriptionPresta_Click(object sender, EventArgs e)
        {

            InscriptionPrestationEntity nouvelleInscription = new InscriptionPrestationEntity();

            nouvelleInscription.DatePrestation = DateTime.Parse(lblDate.Text);
            nouvelleInscription.DateInscription = DateTime.Now;
            nouvelleInscription.CoutMads = int.Parse(lblMads.Text);
            nouvelleInscription.IdPrestation = int.Parse(lblID.Text);
            nouvelleInscription.IdMasseur = int.Parse(Request["idmasseur"]);
            
            
            try
            {
                prestationBU.InscriptionPrestation(nouvelleInscription);
                Response.Redirect("/Prestations/DetailsPrestationInscription.aspx?idprestation=" + idPrestation.ToString() + "&idmasseur=" + Request["idmasseur"] + "&prenom=" + Request["prenom"] + "&nom=" + Request["nom"]);
            }
            catch (Exception exc)
            {
                lblMessage.Text = exc.Message;
                lblMessage.Visible = true;
            }
        }
 

        protected void gvMasseursInscrits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnDesinscriptionPrestations_OnClick")
            {
                prestationBU.DesinscriptionPrestation(int.Parse(e.CommandArgument.ToString()), idPrestation);
                Response.Redirect("/Prestations/DetailsPrestationInscription.aspx?idprestation=" + idPrestation.ToString() + "&idmasseur=" + e.CommandArgument.ToString() + "&prenom=" + Request["prenom"] + "&nom=" + Request["nom"]);
            }
        }
    }
}