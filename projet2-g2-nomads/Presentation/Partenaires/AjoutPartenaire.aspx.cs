﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;

namespace AFCEPF.AI107.Nomads.Presentation.Partenaires
{
    public partial class AjoutPartenaire : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                VilleBU villeBU = new VilleBU();
                List<VilleEntity> villes = villeBU.GetListVille();

                TypePartenaireBU typeBU = new TypePartenaireBU();
                List<TypePartenaireEntity> types = typeBU.GetListTypes();

                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = villes;
                ddlVille.DataBind();

                ddlPartenaire.DataTextField = "Libelle";
                ddlPartenaire.DataValueField = "IdTypePartenaire";
                ddlPartenaire.DataSource = types;
                ddlPartenaire.DataBind();

                JeuxDonneesAjoutPartenaire donneeDemo = new JeuxDonneesAjoutPartenaire();
                PartenaireEntity donneesBar = new PartenaireEntity();
                PartenaireEntity donneesEntreprise = new PartenaireEntity();
                donneesBar = donneeDemo.DemoBar();
                donneesEntreprise = donneeDemo.DemoEntreprise();
                ddlDemoAjoutPartenaire.Items.Add(donneesBar.DenominationSociale);
                ddlDemoAjoutPartenaire.Items.Add(donneesEntreprise.DenominationSociale);


            }
        }


        protected void btnAjout_Click(object sender, EventArgs e)
        {
            PartenaireEntity partenaire = new PartenaireEntity();
            PartenaireBU bu = new PartenaireBU();

            partenaire.DenominationSociale = txtDeno.Text;
            partenaire.Adresse = txtAdresse.Text;
            partenaire.NomContact = txtContact.Text;
            partenaire.MailContact = txtmail.Text;
            partenaire.NumeroContact = txtNumero.Text;
            partenaire.NomSecondaire = txtContactSecondaire.Text;
            partenaire.MailSecondaire = txtMailSecondaire.Text;

            // dateInscription géré en SQL         

            DateTime datePartenariat;
            DateTime.TryParse(txtDebutPartenariat.Text, out datePartenariat);
            partenaire.DateInscription = datePartenariat;

            partenaire.IdVille = int.Parse(ddlVille.SelectedValue);
            partenaire.IdTypePartenaire = int.Parse(ddlPartenaire.SelectedValue);

           // int ville;
           // int.TryParse(intVille.Text, out ville);
           // partenaire.IdVille = ville;

           // int typePartner;
           // int.TryParse(intP.Text, out typePartner);
            //partenaire.IdTypePartenaire = typePartner;
            try { 
            bu.AjouterPartenaire(partenaire);
                lblMessage.Text = "Le partenaire a bien été ajouté!";
                lblMessage.Visible = true;
            } catch (Exception exp)
            {
                lblMessage.Text = exp.Message;
                lblMessage.Visible = true;
            }
            
        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Partenaires/ListePartenaire.aspx");
        }

        protected void btnDemo_Click(object sender, EventArgs e)
        {
            JeuxDonneesAjoutPartenaire donneeDemo = new JeuxDonneesAjoutPartenaire();
            PartenaireEntity donneesBar = new PartenaireEntity();
            PartenaireEntity donneesEntreprise = new PartenaireEntity();
            donneesBar = donneeDemo.DemoBar();
            donneesEntreprise = donneeDemo.DemoEntreprise();

            string partenaireChoisi = ddlDemoAjoutPartenaire.Text;

            if (partenaireChoisi == donneesBar.DenominationSociale)
            {
                txtDeno.Text = donneesBar.DenominationSociale;
                txtAdresse.Text = donneesBar.Adresse;
                txtContact.Text = donneesBar.NomContact;
                txtmail.Text = donneesBar.MailContact;
                txtNumero.Text = donneesBar.NumeroContact;
                txtContactSecondaire.Text = donneesBar.NomSecondaire;
                txtMailSecondaire.Text = donneesBar.MailSecondaire;

                // dateInscription géré en SQL         

                txtDebutPartenariat.Text = donneesBar.DebutPartenariat.ToString("yyyy-MM-dd");
                ddlVille.SelectedValue = donneesBar.IdVille.ToString();
                ddlPartenaire.SelectedValue = donneesBar.IdTypePartenaire.ToString();

                //txtDateNaissance.Text = masseurFemme.DateNaissance.ToString("yyyy-MM-dd");
            }
            if (partenaireChoisi == donneesEntreprise.DenominationSociale)
            {
                txtDeno.Text = donneesEntreprise.DenominationSociale;
                txtAdresse.Text = donneesEntreprise.Adresse;
                txtContact.Text = donneesEntreprise.NomContact;
                txtmail.Text = donneesEntreprise.MailContact;
                txtNumero.Text = donneesEntreprise.NumeroContact;
                txtContactSecondaire.Text = donneesEntreprise.NomSecondaire;
                txtMailSecondaire.Text = donneesEntreprise.MailSecondaire;

                // dateInscription géré en SQL         

                txtDebutPartenariat.Text = donneesEntreprise.DebutPartenariat.ToString("yyyy-MM-dd");
                ddlVille.SelectedValue = donneesEntreprise.IdVille.ToString();
                ddlPartenaire.SelectedValue = donneesEntreprise.IdTypePartenaire.ToString();
            }
        }
    }
}