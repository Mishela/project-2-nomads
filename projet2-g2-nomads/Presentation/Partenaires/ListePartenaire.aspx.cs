﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;

namespace AFCEPF.AI107.Nomads.Presentation.Partenaires
{
    public partial class ListePartenaire : System.Web.UI.Page

    {
        private int id_partenaire;

        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out id_partenaire);

            PartenaireBU bu = new PartenaireBU();
            //List<PartenaireEntity> partenaire = bu.GetListePartenaires();
            
            // pas de if(!isPostBack) nécessaire car si on filtre, on a besoin du postback

                rptPartenaire.DataSource = bu.GetListePartenaires();
                rptPartenaire.DataBind();
            

        }
    }
}
