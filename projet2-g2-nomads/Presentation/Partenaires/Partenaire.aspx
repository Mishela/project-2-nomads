﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Partenaire.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Partenaires.Partenaire" %>

<%@ Register Src="~/Partenaires/UCToggleBarPartenaire.ascx" TagPrefix="uc1" TagName="UCToggleBarPartenaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <uc1:UCToggleBarPartenaire runat="server" ID="UCToggleBarPartenaire" />
    <div class="grid-liste">
        <div class=" masseur liste-item">
            <h1>
                <asp:Label ID="lblDeno" runat="server" Text="Label"></asp:Label>
            </h1>
   
            <h2><asp:Label ID="lblType" runat="server" Text="Label"></asp:Label></h2>
        
            <h3><asp:Label ID="lblVille" runat="server" Text="Label"></asp:Label></h3>

            <p>
                <asp:Label ID="lblAdresse" runat="server" Text="Label"></asp:Label>
            </p>
            <p>
               Inscrit depuis : <asp:Label ID="lblDateInscription" runat="server" Text="Label"></asp:Label>
         
               Date  du premier partenariat : <asp:Label ID="lblDatePartenariat" runat="server" Text="Label"></asp:Label>
            </p>
        
        <table>
            <thead>
               <tr>
                <th colspan="3">Infos Contact</th>
               </tr>
                </thead>
            <tbody>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Numero</th>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNom" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblMailPrincipal" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblNuméro" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNomSecondaire" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblMailSecondaire" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
    
    
        <!-- Après, je vais faire un Tableau avec les contacts
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
        -->
        <asp:Button ID="btnSupprimer" runat="server" OnClick="btnSupprimer_Click" Text="Supprimer" /><asp:Label CssClass="error" ID="lblMessage" Visible="false" runat="server" Text=""></asp:Label>
       </div>
        
        <div class="masseur liste-item">
              <h1 >
                <asp:Label runat="server" Text="lblInscription">Prestations à venir :</asp:Label>
            </h1>
            <br />
            <br />
            <asp:GridView AutoGenerateColumns="false" ID="gvPresta" runat="server">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                <Columns>
                 <asp:BoundField DataField="id_prestation" HeaderText="Référence de la préstation" />
                    <asp:BoundField DataField="libelle_type_prestation" HeaderText="Type préstation" />
                    <asp:BoundField DataField="libelle_prestation" HeaderText="Nom de la préstation" />
                    <asp:BoundField DataField="libelle_ville" HeaderText="Ville" />
                    <asp:BoundField DataField="adresse_prestation" HeaderText="Adresse" />
                    <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:dd MMM yyyy}" />
                </Columns>
            </asp:GridView>
        </div>

                <div class="masseur liste-item">
                      <h1 >
                <asp:Label runat="server" Text="lblInscription">Historique des prestations :</asp:Label>
            </h1>
                    <br /> <br />
            <asp:GridView AutoGenerateColumns="false" ID="gvHistorique" runat="server">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                <Columns>
                 <asp:BoundField DataField="id_prestation" HeaderText="Référence de la préstation" />
                    <asp:BoundField DataField="libelle_type_prestation" HeaderText="Type préstation" />
                    <asp:BoundField DataField="libelle_prestation" HeaderText="Nom de la préstation" />
                    <asp:BoundField DataField="libelle_ville" HeaderText="Ville" />
                    <asp:BoundField DataField="adresse_prestation" HeaderText="Adresse" />
                    <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:dd MMM yyyy}" />
                </Columns>
            </asp:GridView>
        </div>

    </div>
</asp:Content>
