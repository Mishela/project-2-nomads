﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AccueilPartenaire.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Partenaires.AccueilPartenaire" %>

<%@ Register Src="~/Partenaires/UCToggleBarPartenaire.ascx" TagPrefix="uc1" TagName="UCToggleBarPartenaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Accueil Partenaire </h1>
    <h2>Bonjour, ..... ! (partenaire) </h2>
    <uc1:UCToggleBarPartenaire runat="server" ID="UCToggleBarPartenaire" />
</asp:Content>
