﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="ListePartenaire.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Partenaires.ListePartenaire" %>

<%@ Register Src="~/Partenaires/UCToggleBarPartenaire.ascx" TagPrefix="uc1" TagName="UCToggleBarPartenaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPartenaire runat="server" ID="UCToggleBarPartenaire" />
    <asp:Label ID="lblPartenaires" runat="server" Text="Label"><h1>Liste des Partenaires</h1> </asp:Label>
    
    <div class="grid-liste">
        <asp:Repeater ID="rptPartenaire" runat="server">
            <ItemTemplate>

                <div  class="masseur liste-item">
                    <h2><%# DataBinder.Eval(Container.DataItem, "DenominationSociale") %></h2>
                    <h4><%# DataBinder.Eval(Container.DataItem, "TypePartenaire") %></h4>
                    <h4><%# DataBinder.Eval(Container.DataItem, "NomVille") %></h4>
                    <p><%# DataBinder.Eval(Container.DataItem, "Adresse") %></p>
                    <p><%# DataBinder.Eval(Container.DataItem, "NumeroContact") %></p>
                    <a href="Partenaire.aspx?id=<%# DataBinder.Eval(Container.DataItem, "IdPartenaire") %>"> Détails</a>
                </div>

            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
