﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="ListeMasseurs.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.ListeMasseurs" %>

<%@ Register Src="~/Masseurs/UCToggleBarMasseur.ascx" TagPrefix="uc1" TagName="UCToggleBarMasseur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarMasseur runat="server" id="UCToggleBarMasseur" />
    
    <asp:Label ID="lblMasseurActifs" runat="server" Text="Label"> <h1>Masseurs Actifs</h1></asp:Label>
    <!--  Pour factoriser la recherche il faudrait mettre un controleur ici à la place de la nav  -->
    <nav>
         Nom du Masseur : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>         
        Prénom:<asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox>
        Compétence : <asp:DropDownList ID="ddlComp" runat="server"></asp:DropDownList>
        sexe : <asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList>
        Ville : <asp:DropDownList   ID="ddlVille" runat="server"></asp:DropDownList> 
         
        
       <asp:Button ID="btnSearchMasseurs" runat="server" OnClick="btnSearchMasseurs_Click" Text="Chercher des masseurs compatibles" />
    </nav>

    <div class="grid-liste">
    <asp:Repeater ID="rptMasseurs"  runat="server">
        <ItemTemplate >
            

            <div class="masseur liste-item">
                
                <h2><%# DataBinder.Eval(Container.DataItem, "prenom") %> <%# DataBinder.Eval(Container.DataItem, "nom") %></h2>
                <h4>Info Masseur:</h4>
                <p>Sexe : <%# DataBinder.Eval(Container.DataItem, "libelle_sexe") %></p>
                <p>tel: <%# DataBinder.Eval(Container.DataItem, "numero_telephone") %></p>
                <div class="competence">

                </div>
                
                <a href="Masseur.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_masseur") %>">Détails</a>
                <a href="EditionMasseur.aspx?idMasseur=<%# DataBinder.Eval(Container.DataItem, "id_masseur") %>">Modifier</a>
            </div>
           
        </ItemTemplate>
    </asp:Repeater>
    </div>

    <asp:Label ID="lblMasseursInactifs" runat="server" Text="Label"> <h1>Masseurs Inactifs</h1></asp:Label>

<div class="grid-liste">
            <!-- INFOS DU PROFIL -->
            <asp:Repeater ID="rptMasseurInactif" runat="server">
                <ItemTemplate>
                   <div class="masseur liste-item">
                
                <h2><%# DataBinder.Eval(Container.DataItem, "prenom") %> <%# DataBinder.Eval(Container.DataItem, "nom") %></h2>
                <h4>Info Masseur:</h4>
                <p>Sexe : <%# DataBinder.Eval(Container.DataItem, "libelle_sexe") %></p>
                <p>tel: <%# DataBinder.Eval(Container.DataItem, "numero_telephone") %></p>
                <div class="competence">

                </div>
                
                <a href="Masseur.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_masseur") %>">Détails</a>
                <a href="EditionMasseur.aspx?idmasseur=<%# DataBinder.Eval(Container.DataItem, "id_masseur") %>">Modifier</a>
            </div>


                   

                </ItemTemplate>
            </asp:Repeater>
           </div>
    <br />
    <br />

</asp:Content>
