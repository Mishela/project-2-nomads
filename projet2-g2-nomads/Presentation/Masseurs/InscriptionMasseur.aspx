﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="InscriptionMasseur.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.Inscription_Masseur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Inscription Masseur</title>
 
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table">
        <tr>
            <td class="label" >Nom :</td>
            <td><asp:TextBox ID="txtNom" runat="server"  Width="225px"></asp:TextBox>
</td>
        </tr>
        <tr>
            <td class="label" >Prénom :</td>
            <td><asp:TextBox ID="txtPrenom" runat="server" Width="225"></asp:TextBox></td>
        </tr>
         <tr>
            <td class="label"> Date de Naissance :</td>
            <td><asp:TextBox ID="dateDOB" TextMode="Date" runat="server" OnTextChanged="dateDOB_TextChanged" Width="125px"></asp:TextBox></td>
        </tr>

         <tr>
            <td class="label">Sexe :</td>
            <td>
                  <asp:RadioButtonList ID="rblSexe" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSexe_SelectedIndexChanged">
                        <asp:ListItem Selected="False" Value="1"> Masculin </asp:ListItem>
                        <asp:ListItem Selected="False" Value="2"> Féminin </asp:ListItem>
                        <asp:ListItem Selected="False" Value="3"> Indéterminé </asp:ListItem>
                    </asp:RadioButtonList>
        </tr>
         <tr>
            <td class="label">Adresse :</td>
            <td><asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Width="225px"></asp:TextBox></td>
        </tr>
         <tr>
            <td class="label">Code Postal :</td>
            <td><asp:DropDownList ID="ddlCP" runat="server"></asp:DropDownList></td>
        </tr>
         <tr>
            <td class="label">Ville :</td>
            <td><asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList></td>
        </tr>
         <tr>
            <td class="label">Numéro de téléphone :</td>
            <td><asp:TextBox ID="txtTel" runat="server" TextMode="Phone" Width="125px" AutoCompleteType="Cellular"></asp:TextBox>
</td>
        </tr>
         <tr>
            <td class="label">Adresse mail : </td>
            <td><asp:TextBox ID="txtMail" runat="server" TextMode="email" Width="225px"></asp:TextBox></td>
        </tr>
         <tr>
            <td class="label">Mot de Passe : </td>
            <td><asp:TextBox ID="pswdMass" runat="server" TextMode="Password" Width="150px"></asp:TextBox></td>
        </tr>
         <tr>
            <td class="label">Vérifiez votre Mot de Passe : </td>
            <td><asp:TextBox ID="verifPswdMass" runat="server" TextMode="Password" Width="150px"></asp:TextBox></td>
        </tr>
          <tr>
            <td class="label"><asp:Button ID="btnValider" runat="server" Text="Valider" OnClick="btnValider_Click"/></td>
            <td><asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click"/></td>
        </tr>
    </table>

</asp:Content>
