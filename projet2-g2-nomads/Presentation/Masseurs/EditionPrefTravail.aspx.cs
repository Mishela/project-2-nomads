﻿using AFCEPF.AI107.Nomads.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Masseurs
{
    public partial class EditionPrefTravail : System.Web.UI.Page
    {
        private int idMasseur;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idMasseur);
            if (!IsPostBack)
            {
                PrefTravailBU prefTravailBU = new PrefTravailBU();

                gvPrefTravail.DataSource = prefTravailBU.AffichagePrefsMasseur(idMasseur);
                gvPrefTravail.DataBind();


            }

        }
    }
}