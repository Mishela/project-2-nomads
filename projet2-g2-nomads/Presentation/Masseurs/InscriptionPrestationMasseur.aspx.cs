﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Masseurs
{
    public partial class InscriptionPrestationMasseur : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrestationBU bu = new PrestationBU();
                List<PrestationEntity> tournees = new List<PrestationEntity>();

                // DATABINDING : 

                rptInscriptionMasseur.DataSource = bu.GetListPrestations();
                rptInscriptionMasseur.DataBind();
            }

        }

    }
}