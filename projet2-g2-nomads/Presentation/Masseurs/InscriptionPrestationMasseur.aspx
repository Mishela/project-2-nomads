﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="InscriptionPrestationMasseur.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.InscriptionPrestationMasseur" %>

<%@ Register Src="~/Prestations/UCToggleBarPrestation.ascx" TagPrefix="uc1" TagName="UCToggleBarPrestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <uc1:UCToggleBarPrestation runat="server" ID="UCToggleBarPrestation" />
   
    <br />
    <asp:Label class="inscription" ID="inscription" runat="server" Text="Label"> Vous inscrivez : <strong><%=Request["prenom"]%><%=Request["nom"]%></strong></asp:Label>

     <div class="grid-liste">
         
        <asp:Repeater ID="rptInscriptionMasseur" runat="server">
                <ItemTemplate>
                <table class="masseur liste-item">
                    <tr>
                        <td class="FirstCol"><h4><%# DataBinder.Eval(Container.DataItem, "Libelle") %></h4></td>
                        <td class="SecondCol"></td>
                        <td><a href="/Prestations/DetailsPrestationInscription.aspx?idprestation=<%# DataBinder.Eval(Container.DataItem, "IdPrestation")%>&idmasseur=<%=Request["idmasseur"]%>&prenom=<%=Request["prenom"]%>&nom=<%=Request["nom"]%>">Détails</a>
                    </tr>
                    <tr>
                        <td class="FirstCol">Date :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Date", "{0:dd MMM yyyy}") %></td>
                    </tr>
                     <tr>
                        <td class="FirstCol">Adresse :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Adresse") %></td>
                    </tr> 
                    <tr>
                        <td class="FirstCol">Durée :</td>
                        <td class="SecondCol"><%# DataBinder.Eval(Container.DataItem, "Duree") %></td>
                    </tr>   
                    <tr>
                        <td class="FirstCol">Coût en Mads :</td>
                        <td class="SecondCol"><strong> <%# DataBinder.Eval(Container.DataItem, "CoutMads") %></strong></td>
                    </tr> 
                  
                  </table>
            </ItemTemplate>
        </asp:Repeater>
    </div>

</asp:Content>
