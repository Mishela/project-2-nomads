﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Masseurs
{
    public partial class UCPrefTravail : System.Web.UI.UserControl
    {
        private int idMasseur;
        PrefTravailBU prefTravailBU = new PrefTravailBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idMasseur);

            if (!IsPostBack)
            {
                gvPrefTravail.DataSource = prefTravailBU.AffichagePrefsMasseur(idMasseur);
                gvPrefTravail.DataBind();

                List<JourTypeEntity> jours = prefTravailBU.GetListJour();
                ddlJour.DataValueField = "IdJourType";
                ddlJour.DataTextField = "LibelleJourType";
                ddlJour.DataSource = jours;
                ddlJour.DataBind();

            }

        }

        protected void btnValider_Click(object sender, EventArgs e)
        {
            PreferenceTravailEntity preferenceTravail = new PreferenceTravailEntity();

            preferenceTravail.IdJourType = int.Parse(ddlJour.SelectedValue);
            preferenceTravail.DebutValidite = DateTime.Parse(txtDateDebut.Text);
            preferenceTravail.FinValidite = DateTime.Parse(txtDateFin.Text);
            preferenceTravail.HeureDebut = DateTime.Parse(txtHeureDebut.Text);
            preferenceTravail.HeureFin = DateTime.Parse(txtHeureFin.Text);
            preferenceTravail.IdMasseur = idMasseur;

            prefTravailBU.AjouterPrefTravail(preferenceTravail);

            Response.Redirect("Masseur.aspx?id=" + idMasseur.ToString());

        }
    }
}