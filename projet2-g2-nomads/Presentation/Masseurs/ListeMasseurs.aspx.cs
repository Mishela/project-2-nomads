﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class ListeMasseurs : System.Web.UI.Page
    {
        private int idPresta;

        protected void Page_Load(object sender, EventArgs e)
        {

            int.TryParse(Request["id"], out idPresta);

            if (!IsPostBack)
            {
                MasseursBU bu = new MasseursBU();

                // DATABINDING : 

                VilleBU VilleBU = new VilleBU();
                List<VilleEntity> villes = VilleBU.GetListVille();
                villes.Insert(0, new VilleEntity(-1, "ville"));

                SexeBU SexeBU = new SexeBU();
                List<SexeEntity> sexes = SexeBU.GetListSexe();
                sexes.Insert(0, new SexeEntity(-1, "Sexe"));

                CompetenceBU CompBU = new CompetenceBU();
                List<RefCompetenceEntity> competences = CompBU.GetListCompetence();
                competences.Insert(0, new RefCompetenceEntity(-1, "Compétences"));

                // Databinding pour les champs du filtre => 


                ddlVille.DataValueField = "IdVille";
                ddlVille.DataTextField = "Nom";
                ddlVille.DataSource = villes;
                ddlVille.DataBind();

                ddlComp.DataValueField = "IdCompetence";
                ddlComp.DataTextField = "Libelle";
                ddlComp.DataSource = competences;
                ddlComp.DataBind();

                ddlSexe.DataValueField = "IdSexe";
                ddlSexe.DataTextField = "Libelle";
                ddlSexe.DataSource = sexes;
                ddlSexe.DataBind();

                //Databind pour le repeater
                rptMasseurs.DataSource = bu.GetListMasseursActif();
                rptMasseurs.DataBind();

                rptMasseurInactif.DataSource = bu.GetListMasseursInactif();
                rptMasseurInactif.DataBind();
    
            }

        }

        protected void btnSearchMasseurs_Click(object sender, EventArgs e)
        {
            MasseursBU MaBU = new MasseursBU();


            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            int competence = int.Parse(ddlComp.SelectedValue);
            int sexe = int.Parse(ddlSexe.SelectedValue);
            int ville = int.Parse(ddlVille.SelectedValue);

            rptMasseurs.DataSource = MaBU.GetListMasseursActif(nom, prenom, sexe, ville, competence);
            rptMasseurs.DataMember = "id_masseur";
            rptMasseurs.DataBind();
        }
    }
}