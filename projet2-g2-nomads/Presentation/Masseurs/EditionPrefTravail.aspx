﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionPrefTravail.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.EditionPrefTravail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Edition Preférence Travail</h1>

    <asp:GridView ID="gvPrefTravail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display.">
        <Columns>
            <asp:BoundField DataField="libelle_jour_type" HeaderText="libelle_jour_type" SortExpression="libelle_jour_type" />
            <asp:BoundField DataField="debut_validite" HeaderText="debut_validite" SortExpression="debut_validite" />
            <asp:BoundField DataField="fin_validite" HeaderText="fin_validite" SortExpression="fin_validite" />
            <asp:BoundField DataField="heure_debut" HeaderText="heure_debut" SortExpression="heure_debut" />
            <asp:BoundField DataField="heure_fin" HeaderText="heure_fin" SortExpression="heure_fin" />
            
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>












    <!-- Version hard -->
    <!--
    <div class="jour">
        <asp:Label ID="lblLindi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="txtLundiDateDebut" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="txtLundiDateFin" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="txtLundiHeureDebut" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="txtLundiHeureFin" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="lblMardi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="txtMardiDateDebut" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="txtMardiDateFin" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="txtMardiHeureDebut" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="txtMardiHeureFin" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="lblMercredi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="txtMercrediDateDebut" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="txtMercrediDateFin" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="txtMercrediHeureDebut" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="txtMercrediHeureFin" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="lblJeudi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="txtJeudiDateDebut" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="txtJeudiDateFin" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="txtJeudiHeureDebut" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="txtJeudiHeureFin" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="lblVendredi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="txtVendrediDateDebut" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="txtVendrediDateFin" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="txtVendrediHeureDebut" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="txtVendrediHeureFin" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="lblSamedi" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
    </div>
    <div class="jour">
        <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label> Début : <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox> Fin: <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox>
        Heure début : <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox> Heure Fin : <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox>
    </div>-->

</asp:Content>
