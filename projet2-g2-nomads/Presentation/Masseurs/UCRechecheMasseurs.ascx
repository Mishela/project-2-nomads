﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCRechecheMasseurs.ascx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.UCRechecheMasseurs" %>


<div class="champs" >  <!-- la recherche idéale Nom / ville / compétence / sexe  -->
        Nom du Masseur : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
        Prénom : <asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox>
        Compétence : <asp:DropDownList ID="ddlComp" runat="server"></asp:DropDownList>
        Sexe : <asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList>
        Ville : <asp:DropDownList   ID="ddlVille" runat="server"></asp:DropDownList> 
        
       <asp:Button ID="btnSearchMasseurs" runat="server" OnClick="btnSearchMasseurs_Click" Text="Chercher des masseurs compatibles" />

       
    </div>