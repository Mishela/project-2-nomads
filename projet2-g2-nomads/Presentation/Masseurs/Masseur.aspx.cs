﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class Masseur : System.Web.UI.Page
    {
        private int idMasseur;
        MasseursBU masseurBU = new MasseursBU();
        CompetenceBU competenceBU = new CompetenceBU();
        PrestationBU prestationBU = new PrestationBU();
        private int idSuiviCompetence;
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request["id"], out idMasseur);

            if (!IsPostBack)
            {
                //Databind avec le masseurBU
                rptMasseur.DataSource = masseurBU.GetMasseurByIdDataTable(idMasseur);
                rptMasseur.DataBind();


                rptLeftPanel.DataSource = masseurBU.GetMasseurByIdDataTable(idMasseur);
                rptLeftPanel.DataBind();

                gvCompetence.DataSource = competenceBU.GetMasseurCompetences(idMasseur); 
                gvCompetence.DataBind();

                List<RefCompetenceEntity> refCompetences = competenceBU.GetListCompetence();
                ddlCompetence.DataValueField = "IdCompetence";
                ddlCompetence.DataTextField = "Libelle";
                ddlCompetence.DataSource = refCompetences;
                ddlCompetence.DataBind();

                gvInscriptions.DataSource = prestationBU.GetListPrestationsByMasseur(idMasseur);
                gvInscriptions.DataBind();

                gvHistorique.DataSource = prestationBU.GetHistoriqueInscriptionByMasseur(idMasseur);
                gvHistorique.DataBind();

                VerifierActifMasseur(idMasseur);
            }
        }

        protected void btnDesactiverMasseur_Click(object sender, EventArgs e)
        {
            
            MasseurEntity masseur = new MasseurEntity();
            
            masseur.IdMasseur = idMasseur;
            masseur.Desinscription = DateTime.Now;

            masseurBU.DesactiverMasseur(masseur);
            Response.Redirect("/Masseurs/ListeMasseurs.aspx");
        }

        protected void btnActiver_Competence_Click(object sender, EventArgs e)
        {
            SuiviCompetenceEntity suiviCompetence = new SuiviCompetenceEntity();

            suiviCompetence.DateDebut = DateTime.Now;
            suiviCompetence.IdMasseur = idMasseur;
            suiviCompetence.IdRefCompetence = int.Parse(ddlCompetence.SelectedValue);

            try
            {
                competenceBU.ActiverCompetence(suiviCompetence);
                Response.Redirect("/Masseurs/Masseur.aspx?id=" + idMasseur.ToString());

            }
            catch (Exception exc)
            {
                lblErrorCompetence.Visible = true;
                lblErrorCompetence.Text = exc.Message;
            }
        }
        protected void btnReactiver_Click(object sender, EventArgs e)
        {
            MasseurEntity masseur = new MasseurEntity();

            masseur.IdMasseur = idMasseur;
            masseur.Actif = true;

            masseurBU.ReactiverMasseur(masseur);
            Response.Redirect("/Masseurs/ListeMasseurs.aspx");
        }

        protected void gvCompetence_Command(object sender, GridViewCommandEventArgs e)
        {
            int.TryParse(e.CommandArgument.ToString(), out idSuiviCompetence);

            // Command pour désactiver la compétence 
            if (e.CommandName == "Desactiver")   
            {
                SuiviCompetenceEntity suiviCompetence = new SuiviCompetenceEntity();

                suiviCompetence.IdSuiviCompetence = idSuiviCompetence;
                suiviCompetence.IdRefCompetence = int.Parse(competenceBU.GetLigneSuiviCompetence(idSuiviCompetence).IdRefCompetence.ToString());
                suiviCompetence.DateFin = DateTime.Now;
                suiviCompetence.IdMasseur = idMasseur;          // Ici l'id sert pour alimenter l'insert au niveau du BU
                // TODO Creation méthode Desactiver Masseur
                competenceBU.DesactiverCompetence(suiviCompetence);
                Response.Redirect("/Masseurs/Masseur.aspx?id=" + idMasseur.ToString());
            }
        }

        protected void gvInscriptions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "btnDesinscriptionPrestations_OnClick")
            {
                prestationBU.DesinscriptionPrestation(int.Parse(Request["id"]), int.Parse(e.CommandArgument.ToString()));
                Response.Redirect("/Masseurs/Masseur.aspx?id=" + idMasseur.ToString());
            }

        }

     
        
        protected void VerifierActifMasseur(int idMasseur)
        {
            MasseurEntity masseur = new MasseurEntity();

            masseur = masseurBU.GetMasseur(idMasseur);
            if (masseur.Actif == true)
            {
               btnDesactiver.Visible = true;
               btnReactiver.Visible = false;
            }
            if (masseur.Actif == false)
            {
               btnDesactiver.Visible = false;
               btnReactiver.Visible = true;
            }

        }
    }
}
