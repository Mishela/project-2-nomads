﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation.Masseurs
{
    public partial class Inscription_Masseur : System.Web.UI.Page
    {
            VilleBU vBU = new VilleBU();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<VilleEntity> villes = vBU.GetListVille();

                villes.Insert(0, new VilleEntity(-1, "---------", "---------"));

                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = villes;
                ddlVille.DataBind();

                ddlCP.DataTextField = "CodePostal";
                ddlCP.DataValueField = "IdVille";
                ddlCP.DataSource = villes;
                ddlCP.DataBind();
            }
        }

        protected void btnValider_Click(object sender, EventArgs e)
        {

        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/AccueilNomads.aspx");
        }

        protected void dateDOB_TextChanged(object sender, EventArgs e)
        {

        }

        protected void rblSexe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}