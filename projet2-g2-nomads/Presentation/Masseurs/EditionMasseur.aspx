﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="EditionMasseur.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.EditionMasseur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Edition Masseur</h1>
    <div class="masseur-item" id="masseur-prestation">
         <table>
            <tr>
                <td class="FirstCol">Nom :</td>
                <td class="SecondCol"><asp:TextBox ID="txtNom" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="FirstCol">Prenom :</td>
                <td class="SecondCol"><asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox></td>
            </tr>
                <tr>
                <td class="FirstCol">Sexe :</td>
                <td class="SecondCol"><asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList></td>
            </tr> 
            <tr>
                <td class="FirstCol">Date de Naissance :</td>
                <td class="SecondCol"><asp:TextBox ID="txtDateNaissance" runat="server" TextMode="date"></asp:TextBox></td>
            </tr>   
            <tr>
                <td class="FirstCol">Téléphone :</td>
                <td class="SecondCol"><asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox></td>
            </tr>  
             
            <tr>
                <td class="FirstCol">Adresse :</td>
                <td class="SecondCol"><asp:TextBox ID="txtAdress" runat="server"></asp:TextBox></td>
            </tr>
             
            <tr>
                <td class="FirstCol">Ville :</td>
                <td class="SecondCol"><asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList></td>
            </tr>
             
            <tr>
                <td class="FirstCol"></td>
                <td class="SecondCol"><asp:Button ID="btnModifier" runat="server" Text="Enregistrer" OnClick="btnModifier_Click" /></td>
            </tr>
        </table>
    </div>                
</asp:Content>
