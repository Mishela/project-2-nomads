﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPrefTravail.ascx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.UCPrefTravail" %>

 <div>
     <table>
         <thead>
             <tr> 
                 <td> Preference de Travail </td>
             </tr>
         </thead>
         <tr>
             <td>Jour</td>
             <td>Du</td>
             <td> Au </td>
             <td> De </td>
             <td> A </td>
             <td> Valider </td>
         </tr>
         <tr>
             <td><asp:DropDownList ID="ddlJour" runat="server"></asp:DropDownList> </td>
             <td> <asp:TextBox ID="txtDateDebut" runat="server" TextMode="Date"></asp:TextBox> </td>
             <td> <asp:TextBox ID="txtDateFin" runat="server" TextMode="Date"></asp:TextBox> </td>
             <td> <asp:TextBox ID="txtHeureDebut" runat="server" TextMode="Time"></asp:TextBox> </td>
             <td> <asp:TextBox ID="txtHeureFin" runat="server" TextMode="Time"></asp:TextBox> </td>
             <td> <asp:Button ID="btnValider" runat="server" Text="Valider" OnClick="btnValider_Click" /> </td>
         </tr>
     </table>
    </div>
    <asp:GridView ID="gvPrefTravail" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="libelle_jour_type" HeaderText="Jour" />
            <asp:BoundField DataField="debut_validite" HeaderText="Du" />
            <asp:BoundField DataField="fin_validite" HeaderText="Au" />
            <asp:BoundField DataField="heure_debut" HeaderText="De" />
            <asp:BoundField DataField="heure_fin" HeaderText="À" />
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>