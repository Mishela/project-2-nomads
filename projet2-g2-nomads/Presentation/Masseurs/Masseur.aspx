﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Masseur.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseur" %>

<%@ Register Src="~/Masseurs/UCToggleBarMasseur.ascx" TagPrefix="uc1" TagName="UCToggleBarMasseur" %>
<%@ Register Src="~/Masseurs/UCPrefTravail.ascx" TagPrefix="uc1" TagName="UCPrefTravail" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <uc1:UCToggleBarMasseur runat="server" id="UCToggleBarMasseur" />

    <section class="main-container">
        <nav class="left-panel">
            <asp:Repeater ID="rptLeftPanel" runat="server">
                <ItemTemplate>
                    <a href="EditionMasseur.aspx?id=<%# DataBinder.Eval(Container.DataItem, "id_masseur") %>">Modifier</a>
                    |
                    <a  href="/Masseurs/InscriptionPrestationMasseur.aspx?idmasseur=<%# DataBinder.Eval(Container.DataItem, "id_masseur")%>&prenom=<%# DataBinder.Eval(Container.DataItem, "prenom") %>&nom=<%# DataBinder.Eval(Container.DataItem, "nom") %>">Inscription Prestation</a>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                   
               
                </ItemTemplate>
            </asp:Repeater>
             <asp:Button ID="btnDesactiver" runat="server" Text="Désactiver Masseur" OnClick="btnDesactiverMasseur_Click" Visible="true" />
             <asp:Button ID="btnReactiver" Visible="false" runat="server" Text="Réactiver Masseur" OnClick="btnReactiver_Click" />
        </nav>
    

    <div class=" masseur-container">

        <div class="masseur-item" id="masseur-info">
            <!-- INFOS DU PROFIL -->
            <asp:Repeater ID="rptMasseur" runat="server">
                <ItemTemplate>
                    <h1><%# DataBinder.Eval(Container.DataItem, "prenom") %> <%# DataBinder.Eval(Container.DataItem, "nom") %></h1>
                    <h2>Masseur n°<%# DataBinder.Eval(Container.DataItem, "id_masseur") %></h2>
                    <br />

                    <h4>Info Masseur :</h4>
                    <p>Sexe : <%# DataBinder.Eval(Container.DataItem, "libelle_sexe") %></p>
                    <p>Tél : <%# DataBinder.Eval(Container.DataItem, "numero_telephone") %></p>
                    <p>Ville : <%# DataBinder.Eval(Container.DataItem, "libelle_ville") %></p>
                    <p>date de Naissance :   <%# DataBinder.Eval(Container.DataItem, "date_naissance", "{0:dd MMM yyyy}") %></p>
                    <p>Adresse :   <%# DataBinder.Eval(Container.DataItem, "adresse_masseur") %></p>
                    <p>Date d'inscription :   <%# DataBinder.Eval(Container.DataItem, "date_inscription", "{0:dd MMM yyyy}") %></p>
                    <p>Date de désinscription :   <%# DataBinder.Eval(Container.DataItem, "date_desinscription","{0:dd MMM yyyy}") %></p>
                    <p>Nombre de Mads en possession :  <%# DataBinder.Eval(Container.DataItem, "nombre_mads") %></p>


                    <!--    A SORTIR DANS UN UC  -->
                    
                    <!--    A SORTIR DANS UN UC  -->

                </ItemTemplate>
            </asp:Repeater>
           </div>
   
         <!-- PANNEAU DU PROFIL POUR COMPETENCES PREF ET  MADS -->
            <div class="masseur-item" id="masseur-admin">
                <h1>Compétences</h1>
                <br />
                <article>                

            <asp:GridView text="Compétences :" ID="gvCompetence" AutoGenerateColumns="false" runat="server" OnRowCommand="gvCompetence_Command">
                     <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>

                <Columns>
                    <asp:BoundField DataField="libelle_competence" HeaderText="Compétence" />
                    <asp:BoundField DataField="date_debut" HeaderText="Depuis le" DataFormatString ="{0:dd MMM yyyy}"/>
                    
                     <asp:TemplateField>
                        <ItemTemplate>
                             <asp:Button ID="btnDesactiver" runat="server" 
                                        CommandName="Desactiver" 
                                        CommandArgument='<%# Bind("id_suivi_competence") %>' 
                                        Text="Désactiver" />
                            <asp:TextBox ID="Test" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            <asp:DropDownList ID="ddlCompetence" runat="server"></asp:DropDownList>
            <asp:Button ID="btnActiver" runat="server" Text="Activer" OnClick="btnActiver_Competence_Click" />
                    <br />
            <asp:Label ID="lblErrorCompetence" Visible="false" CssClass="error" runat="server" Text=""></asp:Label>

            </article>

                <article>
                    <p>Maecenas sit amet est ut dui luctus blandit. Fusce dui orci, tempus at porta in, ornare vitae elit. Duis a tincidunt turpis. Suspendisse id elementum nisi. Morbi dictum tellus nulla, non rutrum nulla hendrerit eu. Suspendisse sodales convallis mauris, at volutpat libero bibendum ut. Sed nulla metus, fermentum non pellentesque sit amet, placerat eget metus. In volutpat purus sapien, sed iaculis metus mollis placerat. Sed ac tortor sit amet ante fringilla finibus. Pellentesque ornare, sem sit amet tempus porttitor, elit lorem eleifend felis, nec placerat metus nisl quis ex. Nullam vestibulum tempus lorem. Praesent faucibus tortor non arcu interdum, ut hendrerit turpis hendrerit.</p>
                </article>
                <article>
                    <p>Pellentesque nisl est, luctus non auctor sit amet, feugiat vitae leo. Curabitur id lectus eleifend est viverra viverra sit amet et justo. Sed posuere dolor vel dignissim vestibulum. Cras sit amet egestas orci, id sodales eros. Ut tincidunt at dui id aliquam. Cras id nisl a nulla iaculis ornare. Morbi ac tincidunt lectus. Nullam dignissim purus et turpis rhoncus cursus. Donec et erat ut lorem consectetur feugiat in nec felis. Sed sed dictum velit.</p>
                </article>

        </div>

         <!-- UNE DIV POUR INSCRIPTION + HISTORIQUE ????  -->

        <div class="masseur-item" id="masseur-prestation">
            <!-- INSCRIPTIONS DU PROFIL : LES PRESTA A VENIR  -->
            <h1>
                <asp:Label runat="server" Text="lblInscription">Inscriptions :</asp:Label>
            </h1>

            <asp:GridView ID="gvInscriptions" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="false" OnRowCommand="gvInscriptions_RowCommand">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                       <Columns>
                            <asp:BoundField DataField="Libelle" HeaderText="Nom Prestation" />
                            <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString ="{0:dd MMM yyyy}" />
                            <asp:BoundField DataField="Duree" HeaderText="Durée" />
                            <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDesinscriptionPrestation" runat="server" 
                                    CommandName="btnDesinscriptionPrestations_OnClick" 
                                    CommandArgument='<%# Bind("IdPrestation")%>' 
                                    Text="Désinscription" />
                            </ItemTemplate>
                       </asp:TemplateField>
                        </Columns>
            </asp:GridView>
        </div>

    </div>
            <!-- HISTORIQUE DU PROFIL : LES PRESTA EFFECTUEES -->
        <div class="masseur-item" id="masseur-historique">
            <h1 >
                <asp:Label runat="server" Text="lblInscription">Historique :</asp:Label>
             </h1>
            <asp:GridView ID="gvHistorique" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="false">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                        <Columns>
                            <asp:BoundField DataField="Libelle" HeaderText="Nom Prestation" />
                            <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString ="{0:dd MMM yyyy}" />
                            <asp:BoundField DataField="Duree" HeaderText="Durée" />
                         
                        </Columns>

            </asp:GridView>
        </div>

    </section>


    </asp:Content>

