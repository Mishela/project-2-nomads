﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCToggleBarMasseur.ascx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Masseurs.UCToggleBarMasseur" %>

<nav class="underNav">
    <ul>
        <li><a href="/AccueilNomads.aspx">Accueil</a></li>
        <li><a href="ListeMasseurs.aspx">Liste des Masseurs</a></li>
        <li><a href="AjoutMasseur.aspx">Créer un nouveau masseur</a></li>
    </ul>
</nav>