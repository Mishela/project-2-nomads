﻿using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class CompetenceDAO : DAO
    {
        public void Insert(SuiviCompetenceEntity competence)
        {
            IDbCommand cmd = GetCommand(@"insert into suivi_competence
                                            (date_debut, id_ref_competence, id_masseur)
                                            values
                                            (@dateDebut, @idRefCompetence, @idMasseur)");
            cmd.Parameters.Add(new MySqlParameter("@dateDebut", competence.DateDebut));
            cmd.Parameters.Add(new MySqlParameter("@idRefCompetence", competence.IdRefCompetence));
            cmd.Parameters.Add(new MySqlParameter("@idMasseur", competence.IdMasseur));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }


        }
        public void Update(SuiviCompetenceEntity competence)
        {
            IDbCommand cmd = GetCommand(@"UPDATE nomads.suivi_competence SET 
                                        date_fin = @dateFin
                                        WHERE id_suivi_competence = @id;");
            cmd.Parameters.Add(new MySqlParameter("@dateFin", competence.DateFin));
            cmd.Parameters.Add(new MySqlParameter("@id", competence.IdSuiviCompetence));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }

        public DataTable GetCompetencesMasseur(int id)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"select s.date_debut,
                                                s.id_suivi_competence,
                                                s.id_ref_competence,
                                                m.*,
                                                r.libelle_competence
                                                from nomads.suivi_competence s
                                                inner join nomads.masseur m on s.id_masseur = m.id_masseur
                                                left outer join nomads.ref_competence r on s.id_ref_competence = r.id_ref_competence
                                                where m.id_masseur = @id
                                                and (s.date_fin > NOW()
                                                or date_fin is null);");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;
        }

        

        public List<RefCompetenceEntity> GetAllRefCompetence()
        {
            List<RefCompetenceEntity> result = new List<RefCompetenceEntity>();

            IDbCommand cmd = GetCommand(@"select * from ref_competence;");

            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderRefCompetence(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }

        public List<SuiviCompetenceEntity> GetAllActivatedSuiviCompetenceEntity()
        {
            List<SuiviCompetenceEntity> result = new List<SuiviCompetenceEntity>();

            IDbCommand cmd = GetCommand(@"select * from nomads.suivi_competence
                                        where date_fin > now()
                                        or date_fin is null;");

            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderSuivi(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }
        public SuiviCompetenceEntity GetSuiviCompetenceEntityById(int id)
        {
            SuiviCompetenceEntity result = new SuiviCompetenceEntity();

            IDbCommand cmd = GetCommand(@"select * from nomads.suivi_competence
                                        where id_suivi_competence = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderSuivi(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }

        private RefCompetenceEntity DataReaderRefCompetence(IDataReader dr)
        {
            RefCompetenceEntity refCompetence = new RefCompetenceEntity();

            refCompetence.IdCompetence = dr.GetInt32(dr.GetOrdinal("id_ref_competence"));
            refCompetence.Libelle = dr.GetString(dr.GetOrdinal("libelle_competence"));

            return refCompetence;
        }

        private SuiviCompetenceEntity DataReaderSuivi(IDataReader dr)
        {
            SuiviCompetenceEntity suiviCompetence = new SuiviCompetenceEntity();
            suiviCompetence.IdSuiviCompetence = dr.GetInt32(dr.GetOrdinal("id_suivi_competence"));
            suiviCompetence.DateDebut = dr.GetDateTime(dr.GetOrdinal("date_debut"));
            // Date de fin peut être null
            if (!dr.IsDBNull(dr.GetOrdinal("date_fin"))) suiviCompetence.DateFin = dr.GetDateTime(dr.GetOrdinal("date_fin"));
            suiviCompetence.IdRefCompetence = dr.GetInt32(dr.GetOrdinal("id_ref_competence"));
            suiviCompetence.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));

            return suiviCompetence;
        }
    }

}
