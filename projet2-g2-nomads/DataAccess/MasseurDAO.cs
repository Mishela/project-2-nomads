﻿using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class MasseurDAO : DAO
    {
        public List<MasseurEntity> GetAllMasseur()
        {
            List<MasseurEntity> result = new List<MasseurEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"SELECT * FROM masseur");

            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {
                    
                    result.Add(DataReaderToEntity(dr));

                }


            }catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();

            }

            return result;
        }


        public List<MasseurEntity> GetAllMasseurByPrestation(int idPrestation)
        {
            List<MasseurEntity> result = new List<MasseurEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"  SELECT *
                                            FROM nomads.masseur m
                                            JOIN nomads.inscription_prestation i on m.id_masseur = i.id_masseur
                                            JOIN nomads.prestation p on p.id_prestation = i.id_prestation
                                            WHERE p.id_prestation = @idprestation
                                            ORDER BY m.nom;");
            cmd.Parameters.Add(new MySqlParameter("@idprestation", idPrestation));

            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }
        public DataTable GetActivatedMasseurs()
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select m.*,
                                        s.libelle_sexe,
                                        v.libelle_ville,
                                        v.code_postal
                                        FROM nomads.masseur m
                                        INNER JOIN nomads.sexe s on m.id_sexe = s.id_sexe
                                        LEFT OUTER JOIN nomads.ville v on m.id_ville = v.id_ville
                                        WHERE m.date_desinscription is null
                                        ORDER BY m.nom;");

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;
        }

        // surcharge
        public DataTable GetActivatedMasseurs(string nom, string prenom, int sexe, int ville, int competence)
        {
            DataTable result = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT ma.*, competence.libelle_competence, sexe.libelle_sexe, ville.libelle_ville 
                                           FROM masseur as ma JOIN sexe ON ma.id_sexe = sexe.id_sexe 
                                           JOIN Ville ON ma.id_ville = ville.id_ville 
                                           LEFT JOIN (SELECT suivi_competence.id_masseur, libelle_competence,  ref_competence.id_ref_competence
                                                            FROM suivi_competence 
                                                            JOIN ref_competence ON suivi_competence.id_ref_competence = ref_competence.id_ref_competence) competence ON ma.id_masseur = competence.id_masseur 
                                        
                                         WHERE ma.nom LIKE @nom
                                         AND ma.prenom LIKE @prenom
                                         AND (@idCompetence = -1 OR competence.id_ref_competence = @idCompetence)
                                         AND (@idVille = -1 OR ville.id_ville = @idVille)
                                         AND (@idSexe = -1 OR sexe.id_sexe = @idSexe)
                                        ORDER BY libelle_ville;");


            cmd.Parameters.Add(new MySqlParameter("@nom", "%" + nom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@prenom", "%" + prenom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@idVille", ville));
            cmd.Parameters.Add(new MySqlParameter("@idSexe", sexe));
            cmd.Parameters.Add(new MySqlParameter("@idCompetence", competence));

            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;
        }

        public void SoustractionMads(int idMasseur, int nbMads)
        {
           
                IDbCommand cmd = GetCommand(@"UPDATE masseur SET
                                            nombre_mads = @nbMads
                                            WHERE id_masseur = @id;");

                cmd.Parameters.Add(new MySqlParameter("@id", idMasseur));
                cmd.Parameters.Add(new MySqlParameter("@nbMads", nbMads));

            try
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    throw exc;
                }
                finally
                {
                    cmd.Connection.Close();
                }
        }

        public DataTable GetMasseurActiveById(int id)
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select m.*,
                                        s.libelle_sexe,
                                        v.libelle_ville,
                                        v.code_postal
                                        FROM nomads.masseur m
                                        INNER JOIN nomads.sexe s on m.id_sexe = s.id_sexe
                                        LEFT OUTER JOIN nomads.ville v on m.id_ville = v.id_ville
                                        where m.date_desinscription is null
                                        and id_masseur = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;

        }

        public MasseurEntity GetById(int id)
        {
            MasseurEntity result = null;
            // preparer la commande
            IDbCommand cmd = GetCommand(@"SELECT * 
                                        FROM masseur
                                        WHERE id_masseur = @id");
            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // Ouvrir la connection 
                cmd.Connection.Open();

                //Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }

            }catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }


            return result;
        }

        public void Insert(MasseurEntity masseur)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO masseur
                                        (nom, prenom, numero_telephone, date_naissance, adresse_masseur, date_inscription, id_sexe, id_ville)
                                        VALUES
                                        (@nom, @prenom, @telephone, @dateNaissance, @adresse, @dateInscription, @idSexe, @idVille);
                                        SELECT LAST_INSERT_ID();");
            cmd.Parameters.Add(new MySqlParameter("@nom", masseur.Nom));
            cmd.Parameters.Add(new MySqlParameter("@prenom", masseur.Prenom));
            cmd.Parameters.Add(new MySqlParameter("@telephone", masseur.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@dateNaissance", masseur.DateNaissance));
            cmd.Parameters.Add(new MySqlParameter("@adresse", masseur.Adresse));
            cmd.Parameters.Add(new MySqlParameter("@dateInscription", masseur.Inscription));
            cmd.Parameters.Add(new MySqlParameter("@idSexe", masseur.IdSexe));
            cmd.Parameters.Add(new MySqlParameter("@idVille", masseur.IdVille));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Update(MasseurEntity masseur)
        {
            IDbCommand cmd = GetCommand(@"UPDATE masseur SET
                                            nom = @nom,
                                            prenom = @prenom,
                                            numero_telephone = @telephone,
                                            date_naissance = @dateNaissance,
                                            adresse_masseur = @adresse,
                                            id_sexe = @idSexe,
                                            id_ville = @idVille
                                            WHERE id_masseur = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", masseur.IdMasseur));
            cmd.Parameters.Add(new MySqlParameter("@nom", masseur.Nom));
            cmd.Parameters.Add(new MySqlParameter("@prenom", masseur.Prenom));
            cmd.Parameters.Add(new MySqlParameter("@telephone", masseur.NumeroTelephone));
            cmd.Parameters.Add(new MySqlParameter("@dateNaissance", masseur.DateNaissance));
            cmd.Parameters.Add(new MySqlParameter("@adresse", masseur.Adresse));
            
            cmd.Parameters.Add(new MySqlParameter("@idSexe", masseur.IdSexe));
            cmd.Parameters.Add(new MySqlParameter("@idVille", masseur.IdVille));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public void Desactivation(MasseurEntity masseur)
        {
            IDbCommand cmd = GetCommand(@"UPDATE masseur SET
                                            date_desinscription = @dateDesinscription,
                                            actif_masseur = false
                                            WHERE id_masseur = @id;");

            cmd.Parameters.Add(new MySqlParameter("@id", masseur.IdMasseur));
            cmd.Parameters.Add(new MySqlParameter("@dateDesinscription", masseur.Desinscription));
            
            
            

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }

        public void Reactivation(MasseurEntity masseur)
        {
            IDbCommand cmd = GetCommand(@"UPDATE masseur SET
                                            date_desinscription = NULL,
                                            actif_masseur = true
                                            WHERE id_masseur = @id;");

            cmd.Parameters.Add(new MySqlParameter("@id", masseur.IdMasseur));
           // cmd.Parameters.Add(new MySqlParameter("@dateDesinscription", "''"));
           



            try
            {
                cmd.Connection.Open();
                masseur.IdMasseur = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

        }
        private MasseurEntity DataReaderToEntity(IDataReader dr)
        {
            MasseurEntity masseur = new MasseurEntity();

            masseur.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));
            masseur.Nom = dr.GetString(dr.GetOrdinal("nom"));
            masseur.Prenom = dr.GetString(dr.GetOrdinal("prenom"));
            masseur.NumeroTelephone = dr.GetString(dr.GetOrdinal("numero_telephone"));
            masseur.DateNaissance = dr.GetDateTime(dr.GetOrdinal("date_naissance"));
            masseur.IdSexe = dr.GetInt32(dr.GetOrdinal("id_sexe")); // Formatage ou Datatable à prévoir afin de récupérer le libellé dans d'autre table
            masseur.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            masseur.Adresse = dr.GetString(dr.GetOrdinal("adresse_masseur"));
            masseur.Inscription = dr.GetDateTime(dr.GetOrdinal("date_inscription"));
            masseur.NombreMads = dr.GetInt32(dr.GetOrdinal("nombre_mads"));
            masseur.Actif = dr.GetBoolean(dr.GetOrdinal("actif_masseur"));

            if (!dr.IsDBNull(dr.GetOrdinal("date_desinscription")))                 // Date de désinscription peut être null
            {
                masseur.Desinscription =  dr.GetDateTime(dr.GetOrdinal("date_desinscription"));
            }

            return masseur;
        }
        public DataTable GetInactivatedMasseurs()
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select m.*,
                                        s.libelle_sexe,
                                        v.libelle_ville,
                                        v.code_postal
                                        FROM nomads.masseur m
                                        INNER JOIN nomads.sexe s on m.id_sexe = s.id_sexe
                                        LEFT OUTER JOIN nomads.ville v on m.id_ville = v.id_ville
                                        WHERE m.date_desinscription IS NOT NULL
                                        ORDER BY m.nom;");
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);


            return result;
        }
        public DataTable GetMasseurInactifById(int id)
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select m.*,
                                        s.libelle_sexe,
                                        v.libelle_ville,
                                        v.code_postal
                                        FROM nomads.masseur m
                                        INNER JOIN nomads.sexe s on m.id_sexe = s.id_sexe
                                        LEFT OUTER JOIN nomads.ville v on m.id_ville = v.id_ville
                                        where m.date_desinscription IS NOT NULL
                                        and id_masseur = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;

        }
        public DataTable GetAllMasseurById(int id)
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select m.*,
                                        s.libelle_sexe,
                                        v.libelle_ville,
                                        v.code_postal
                                        FROM nomads.masseur m
                                        INNER JOIN nomads.sexe s on m.id_sexe = s.id_sexe
                                        LEFT OUTER JOIN nomads.ville v on m.id_ville = v.id_ville
                                        WHERE id_masseur = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;
        }

    }
}
