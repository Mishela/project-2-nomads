﻿using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class SexeDAO : DAO
    {
        public List<SexeEntity> GetAllSexe()
        {
            List<SexeEntity> result = new List<SexeEntity>();
            // Prepare la command
            IDbCommand cmd = GetCommand(@"SELECT * FROM sexe");

            try
            {
                // Ouverture connection 
                cmd.Connection.Open();

                //Exécution command
                IDataReader dr = cmd.ExecuteReader();
                // Traitement Command
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }catch(Exception exc)
            {
                // Fermeture Connection 
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        public SexeEntity GetById(int id)
        {
            SexeEntity result = new SexeEntity();

            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM sexe
                                            WHERE id_sexe = @id");
            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // Ouverture connection 
                cmd.Connection.Open();

                //Exécution command
                IDataReader dr = cmd.ExecuteReader();
                // Traitement Command
               
                result = DataReaderToEntity(dr);
           
            }
            catch (Exception exc)
            {
                // Fermeture Connection 
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        private SexeEntity DataReaderToEntity(IDataReader dr)
        {
            SexeEntity sexe = new SexeEntity();

            sexe.IdSexe = dr.GetInt32(dr.GetOrdinal("id_sexe"));
            sexe.Libelle = dr.GetString(dr.GetOrdinal("libelle_sexe"));

            return sexe;
        }
    }
}
