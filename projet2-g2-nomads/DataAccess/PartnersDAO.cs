﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System.Data;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class PartnersDAO : DAO
    {
        #region READ
        public List<PartenaireEntity> GetAllPartenaire()
        {

            List<PartenaireEntity> result = new List<PartenaireEntity>();
            //prep commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * FROM partenaire
                                        JOIN nomads.type_partenaire ON  partenaire.id_type_partenaire = type_partenaire.id_type_partenaire
                                        JOIN nomads.ville ON  partenaire.id_ville = ville.id_ville
                                        ORDER BY denomination_sociale
                                        ");

            try
            {
                // 3 ouvrir connection
                cmd.Connection.Open();

                // 4 Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }


            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

         public PartenaireEntity GetPartenaireById(int id) 
        {
            PartenaireEntity result = null;

            //prep commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.partenaire 
                                            
                                            JOIN nomads.type_partenaire ON  partenaire.id_type_partenaire = type_partenaire.id_type_partenaire 
                                            JOIN nomads.ville ON  partenaire.id_ville = ville.id_ville
                                            WHERE partenaire.id_partenaire = @id
                                                   ");
           
            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // 3 ouvrir connection
                cmd.Connection.Open();

                // 4 Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 Traiter le résultat
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        
        // Pas utilisé = récupère le type partenaire et libelle ville et les met dans une datatable
            public DataTable GetcompInfo()
        {
            DataTable result = new DataTable();
            

            IDbCommand cmd = GetCommand(@"SELECT libelle_type_partenaire, libelle_ville 
                                FROM nomads.partenaire 
                                JOIN nomads.type_partenaire ON  partenaire.id_type_partenaire = type_partenaire.id_type_partenaire 
                                JOIN nomads.ville ON  partenaire.id_ville = ville.id_ville;
                                                                                             ");
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;

            // remplir la datatable :
            da.Fill(result);

            return result;

        }
        

        #endregion

        #region CREATE

        public void AddPartenaire(PartenaireEntity partenaire) 
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO partenaire
                    (Denomination_sociale, adresse_partenaire, date_inscription, debut_partenariat, nom_contact, mail_contact, id_ville, id_type_partenaire )
                    VALUES 
                    (@deno, @adresse, NOW(), @dp, @contact, @mail, @idVille, @typePartner )");

            cmd.Parameters.Add(new MySqlParameter("@deno", partenaire.DenominationSociale));
            cmd.Parameters.Add(new MySqlParameter("@adresse", partenaire.Adresse));
            cmd.Parameters.Add(new MySqlParameter("@inscription", partenaire.DateInscription));
            cmd.Parameters.Add(new MySqlParameter("@dp", partenaire.DebutPartenariat));
            cmd.Parameters.Add(new MySqlParameter("@contact", partenaire.NomContact));
            cmd.Parameters.Add(new MySqlParameter("@mail", partenaire.MailContact));
            cmd.Parameters.Add(new MySqlParameter("@idVille", partenaire.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@typePartner", partenaire.IdTypePartenaire));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        #endregion

        #region DELETE
        public void DeletePartner(int id) {
            IDbCommand cmd = GetCommand(@"DELETE FROM partenaire
                                                   WHERE id_partenaire = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // 3 ouvrir connection
                cmd.Connection.Open();

                // 4 Exécuter la commande
                cmd.ExecuteNonQuery();

                
            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        #endregion

        #region Update
        // public ModifyPartner(){} 
        #endregion

        //méthode de factorisation pour le dataReader
        private PartenaireEntity DataReaderToEntity(IDataReader dr)
        {
            PartenaireEntity partenaire = new PartenaireEntity();
            //TypePartenaireEntity typePartenaire = new TypePartenaireEntity();
            // CHAMPS OBLIGATOIRES 
            partenaire.IdPartenaire = dr.GetInt32(dr.GetOrdinal("id_partenaire"));
            partenaire.DenominationSociale = dr.GetString(dr.GetOrdinal("Denomination_sociale"));
            
            partenaire.Adresse = dr.GetString(dr.GetOrdinal("adresse_partenaire"));
            partenaire.DebutPartenariat = dr.GetDateTime(dr.GetOrdinal("debut_partenariat"));
            partenaire.NomContact = dr.GetString(dr.GetOrdinal("nom_contact"));
            partenaire.MailContact = dr.GetString(dr.GetOrdinal("mail_contact"));
            partenaire.DateInscription = dr.GetDateTime(dr.GetOrdinal("date_inscription"));

            //Valeurs pour les jointures
            if (!dr.IsDBNull(dr.GetOrdinal("id_ville")))
            {
                partenaire.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("libelle_ville")))
            {
                partenaire.NomVille = dr.GetString(dr.GetOrdinal("libelle_ville"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_type_partenaire")))
            {
                partenaire.IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("libelle_type_partenaire")))
            {
                partenaire.TypePartenaire = dr.GetString(dr.GetOrdinal("libelle_type_partenaire"));
            }

           // partenaire.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            //partenaire.NomVille = dr.GetString(dr.GetOrdinal("libelle_ville"));

          //  partenaire.IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire"));
          //  partenaire.TypePartenaire = dr.GetString(dr.GetOrdinal("libelle_type_partenaire"));
            



            // NON OBLIGATOIRES
            if (!dr.IsDBNull(dr.GetOrdinal("fin_partenariat")))
            {
                partenaire.FinPartenariat = dr.GetDateTime(dr.GetOrdinal("fin_partenariat"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("numero_contact")))
            {
                partenaire.NumeroContact = dr.GetString(dr.GetOrdinal("numero_contact"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("nom_secondaire")))
            {
                partenaire.NomSecondaire = dr.GetString(dr.GetOrdinal("nom_secondaire"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("mail_secondaire")))
            {
                partenaire.MailSecondaire = dr.GetString(dr.GetOrdinal("mail_secondaire"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_sortie")))
            {
                partenaire.DateSortie = dr.GetDateTime(dr.GetOrdinal("date_sortie"));
            }

            // partenaire.idprestation   PAS SUR DE CELLE LA.

            return partenaire;
        }
    }
}
