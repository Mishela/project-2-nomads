﻿using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class PreferenceTravailDAO : DAO
    {
        public DataTable GetAllPrefMasseur(int id)
        {
            DataTable result = new DataTable();
            IDbCommand cmd = GetCommand(@"select p.*,
		                                j.libelle_jour_type
                                        from nomads.preference_travail p 
                                        inner join nomads.jour_type j on p.id_jour_type = j.id_jour_type
                                        where p.id_masseur = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            MySqlDataAdapter da = new MySqlDataAdapter();
            da.SelectCommand = (MySqlCommand)cmd;
            da.Fill(result);

            return result;
        }
        public List<PreferenceTravailEntity> GetPrefsTravailMasseur(int id)
        {
            List<PreferenceTravailEntity> result = new List<PreferenceTravailEntity>();
            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.preference_travail
                                        WHERE id_masseur = @id;");
            cmd.Parameters.Add(new MySqlParameter("@id", id));
            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderToPrefTravail(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }


            return result;
        }
        public List<PreferenceTravailEntity> GetAllPreferenceTravail()
        {
            List<PreferenceTravailEntity> result = new List<PreferenceTravailEntity>();
            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.preference_travail;");

            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderToPrefTravail(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }

        public PreferenceTravailEntity GetPreferenceTravailById(int id) // pour cibler une ligne par l'idprefTravail
        {
            PreferenceTravailEntity result = new PreferenceTravailEntity();



            return result;
        }

        public List<JourTypeEntity> GetAllJourTypes()
        {
            List<JourTypeEntity> result = new List<JourTypeEntity>();
            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.jour_type;");

            try
            {
                cmd.Connection.Open();
                IDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderToJourTypeEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }


            return result;
        }

        public void Insert(PreferenceTravailEntity preferences)
        {
            

                IDbCommand cmd = GetCommand(@"insert into preference_travail
                                            (debut_validite, fin_validite heure_debut, heure_fin, id_jour_type, id_masseur)
                                            values
                                            (@debutValidite, @finValidite, @heureDebut, @heureFin, @idJour, @idMasseur)");
                cmd.Parameters.Add(new MySqlParameter("@debutValidite", preferences.DebutValidite));
                cmd.Parameters.Add(new MySqlParameter("@finValidite", preferences.FinValidite));
                cmd.Parameters.Add(new MySqlParameter("@heureDebut", preferences.HeureDebut));
                cmd.Parameters.Add(new MySqlParameter("@heureFin", preferences.HeureFin));
                cmd.Parameters.Add(new MySqlParameter("@idJour", preferences.IdJourType));
                cmd.Parameters.Add(new MySqlParameter("@idMasseur", preferences.IdMasseur));

                try
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    throw exc;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            
        }
        public void Insert(List<PreferenceTravailEntity> preferences)
        {
            foreach(PreferenceTravailEntity p in preferences)
            {

                IDbCommand cmd = GetCommand(@"insert into preference_travail
                                            (debut_validite, heure_debut, heure_fin, id_jour_type, id_masseur)
                                            values
                                            (@debutValidite, @heureDebut, @heureFin, @idJour, @idMasseur)");
                cmd.Parameters.Add(new MySqlParameter("@debutValidite", p.DebutValidite));
                cmd.Parameters.Add(new MySqlParameter("@heureDebut", p.HeureDebut));
                cmd.Parameters.Add(new MySqlParameter("@heureFin", p.HeureFin));
                cmd.Parameters.Add(new MySqlParameter("@idJour", p.IdJourType));
                cmd.Parameters.Add(new MySqlParameter("@idMasseur", p.IdMasseur));

                try
                {
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    throw exc;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        private PreferenceTravailEntity DataReaderToPrefTravail(IDataReader dr)
        {
            PreferenceTravailEntity preference = new PreferenceTravailEntity();

            preference.IdPrefTravail = dr.GetInt32(dr.GetOrdinal("id_pref_travail"));
            preference.DebutValidite = dr.GetDateTime(dr.GetOrdinal("debut_validite"));
            if (!dr.IsDBNull(dr.GetOrdinal("fin_validite"))) preference.FinValidite = dr.GetDateTime(dr.GetOrdinal("fin_validite"));
            preference.HeureDebut = dr.GetDateTime(dr.GetOrdinal("heure_debut"));
            preference.HeureFin = dr.GetDateTime(dr.GetOrdinal("heure_fin"));
            preference.IdJourType = dr.GetInt32(dr.GetOrdinal("id_jour_type"));
            preference.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));


            return preference;
        }
        private JourTypeEntity DataReaderToJourTypeEntity(IDataReader dr)
        {
            JourTypeEntity jour = new JourTypeEntity();
            jour.IdJourType = dr.GetInt32(dr.GetOrdinal("id_jour_type"));
            jour.LibelleJourType = dr.GetString(dr.GetOrdinal("libelle_jour_type"));

            return jour;
        }
    }
}
