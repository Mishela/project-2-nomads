﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public abstract class DAO
    {
        private const string connectionString = "Server=localhost;Database=nomads;Uid=root;Pwd=root;";

        protected IDbCommand GetCommand(string sql)
        {
            IDbConnection cnx =new  MySqlConnection();
            cnx.ConnectionString = connectionString;

            IDbCommand cmd = new MySqlCommand();
            cmd.Connection = cnx;
            cmd.CommandText = sql;

            return cmd;
        }
    }
}
