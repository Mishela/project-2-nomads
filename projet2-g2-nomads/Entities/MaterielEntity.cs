﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class MaterielEntity
    {
        public int IdMateriel { get; set; }
        public string NomMateriel { get; set; }
        public DateTime DateAchat { get; set; }
        public int? IdTypeMateriel { get; set; }

    }
}
