﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class SexeEntity
    {
        public int IdSexe { get; set; }
        public string Libelle { get; set; }

        public SexeEntity()
        {
           

        }

        public SexeEntity(int id, string libelle)
    {
        IdSexe = id;
        Libelle = libelle;

    }
    }
}
