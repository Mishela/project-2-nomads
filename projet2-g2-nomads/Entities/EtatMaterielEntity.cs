﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class EtatMaterielEntity
    {
        public int IdEtatMateriel { get; set; }
        public string LibelleEtat { get; set; }

    }
}
