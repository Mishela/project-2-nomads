﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class PrestationEntity
    {
        public int IdPrestation { get; set; }
        public string Libelle { get; set; }
        public DateTime Date { get; set; }
        public string Duree { get; set; }
        public int MasseurMin { get; set; }
        public int MasseurMax { get; set; }
        public int CoutMads { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime DateAnnulation { get; set; }
        public string Adresse { get; set; }
        public string LibelleFeedBack { get; set; }
        public DateTime DateFeedBack { get; set; }
        public double MontantDevis { get; set; }
        public DateTime DateDevis { get; set; }
        public double MontantFacturer { get; set; }
        public int NoteFeedBack { get; set; }
        public int IdPartenaire { get; set; }
        public int IdTypeRecurrence { get; set; }
        public int IdTypePrestation { get; set; }
        public int IdTypeAnnulation { get; set; }
        public int IdVille { get; set; }
       
    }
}
