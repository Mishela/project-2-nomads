﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypePrestationEntity
    {
        public int IdTypePrestation { get; set; }
        public string LibellePrestation { get; set; }

        public TypePrestationEntity()
        {
           
        }
        public TypePrestationEntity (int id, string libelle){
            IdTypePrestation = id;
            LibellePrestation = libelle;
        }

    }
}
