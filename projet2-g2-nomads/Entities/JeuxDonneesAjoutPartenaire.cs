﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class JeuxDonneesAjoutPartenaire
    {
        public PartenaireEntity DemoBar()
        {
            PartenaireEntity demoBar = new PartenaireEntity();

            demoBar.DenominationSociale = "Le Social Bar";
            demoBar.Adresse = "25, Rue Villiot";
            demoBar.DebutPartenariat = DateTime.Now.AddDays(+4);
            demoBar.IdVille = 1;
            demoBar.IdTypePartenaire = 1;
            demoBar.NomContact = "ABDELATIF";
            demoBar.MailContact = "contact@social-bar.org";
            demoBar.NumeroContact = "01.49.48.47.46";
            demoBar.NomSecondaire = "BONCHE";
            demoBar.MailSecondaire = "contact2@social-bar.org";
            return demoBar;

        }

        public PartenaireEntity DemoEntreprise()
        {
            PartenaireEntity demoBar = new PartenaireEntity();

            demoBar.DenominationSociale = "Capgemini";
            demoBar.Adresse = "11 rue de Tilsitt";
            demoBar.DebutPartenariat = DateTime.Now.AddDays(+5);
            demoBar.IdVille = 1;
            demoBar.IdTypePartenaire = 3;
            demoBar.NomContact = "DECLEIRE";
            demoBar.MailContact = "contact@capgemini.org";
            demoBar.NumeroContact = "01.23.24.25.26";
            demoBar.NomSecondaire = "DUMONT";
            demoBar.MailSecondaire = "contact2@capgemini.org";
            return demoBar;

        }
    }
}
