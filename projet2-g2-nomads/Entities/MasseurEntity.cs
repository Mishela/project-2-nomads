﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class MasseurEntity
    {
        public int IdMasseur { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string NumeroTelephone { get; set; }
        public DateTime DateNaissance { get; set; }
        public string Adresse { get; set; }
        public DateTime Inscription { get; set; }
        public DateTime? Desinscription { get; set; }
        public int? IdInscriptionPrestation { get; set; }
        public int? IdTransaction { get; set; }
        public int? IdPeriodIndispo { get; set; }
        public int? IdPreferenceTravail { get; set; }
        public int IdSexe { get; set; }
        public string Sexe { get; set; }
        public int? IdSuiviCompetence { get; set; }
        public int? IdVilleIntervention { get; set; }
        public int? IdVille { get; set; }
        public string Ville { get; set; }
        public string CodePostal { get; set; }
        public int NombreMads { get; set; }
        public bool Actif { get; set; }
        
    }


}
