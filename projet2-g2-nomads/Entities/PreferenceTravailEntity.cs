﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class PreferenceTravailEntity
    {
        public int IdPrefTravail { get; set; }
        public DateTime DebutValidite { get; set; }
        public DateTime FinValidite { get; set; }
        public DateTime HeureDebut { get; set; }
        public DateTime HeureFin { get; set; }
        public int IdMasseur { get; set; }
        public int IdJourType { get; set; }

    }
}
