﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypeMaterielEntity
    {
        public int IdTypeMateriel { get; set; }
        public string LibelleMateriel { get; set; }
        public string EtatMateriel { get; set; }
        public int? IdPrestation { get; set; }

    }
}
