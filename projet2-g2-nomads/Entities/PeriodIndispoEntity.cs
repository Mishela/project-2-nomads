﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class PeriodIndispoEntity
    {
        public int IdPeriodIndispo { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }

    }
}
