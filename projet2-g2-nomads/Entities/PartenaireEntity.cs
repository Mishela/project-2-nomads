﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class PartenaireEntity
    {
        public int IdPartenaire { get; set; }
        public string DenominationSociale { get; set; }
        public string Adresse { get; set; }
        public DateTime DebutPartenariat { get; set; }
        public DateTime FinPartenariat { get; set; }
        public string NomContact { get; set; }
        public string NumeroContact { get; set; }
        public string MailContact { get; set; }
        public string NomSecondaire { get; set; }
        public string MailSecondaire { get; set; }
        public DateTime DateInscription { get; set; }
        public DateTime DateSortie { get; set; }
        public int IdVille { get; set; }
        public string NomVille { get; set; }
        public int IdTypePartenaire { get; set; }
        public string TypePartenaire { get; set; }

        public int IdPrestation { get; set; }       // ici une list d'IdPrestation normalement
    }
}
