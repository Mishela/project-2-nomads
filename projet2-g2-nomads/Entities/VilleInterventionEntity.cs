﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class VilleInterventionEntity
    {
        public int IdVilleIntervention { get; set; }
        public DateTime DateChoix { get; set; }
        public DateTime DateFinChoix { get; set; }
        public int IdMasseur { get; set; }
        public int IdVille { get; set; }
    }
}
