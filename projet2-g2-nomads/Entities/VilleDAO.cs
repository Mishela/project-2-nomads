﻿using AFCEPF.AI107.Nomads.Entities;
#pragma warning disable CS0246 // Le nom de type ou d'espace de noms 'MySql' est introuvable (vous manque-t-il une directive using ou une référence d'assembly ?)
using MySql.Data.MySqlClient;
#pragma warning restore CS0246 // Le nom de type ou d'espace de noms 'MySql' est introuvable (vous manque-t-il une directive using ou une référence d'assembly ?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
#pragma warning disable CS0246 // Le nom de type ou d'espace de noms 'DAO' est introuvable (vous manque-t-il une directive using ou une référence d'assembly ?)
    public class VilleDAO : DAO
#pragma warning restore CS0246 // Le nom de type ou d'espace de noms 'DAO' est introuvable (vous manque-t-il une directive using ou une référence d'assembly ?)
    {
        public List<VilleEntity> GetAll()
        {
            List<VilleEntity> result = new List<VilleEntity>();
            // Prepare la command
            IDbCommand cmd = GetCommand(@"SELECT * FROM ville");

            try
            {
                // Ouverture connection 
                cmd.Connection.Open();

                //Exécution command
                IDataReader dr = cmd.ExecuteReader();
                // Traitement Command
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                // Fermeture Connection 
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        public VilleEntity GetById(int id)
        {
            VilleEntity result = new VilleEntity();

            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM ville
                                            WHERE id_ville = @id");
            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // Ouverture connection 
                cmd.Connection.Open();

                //Exécution command
                IDataReader dr = cmd.ExecuteReader();
                // Traitement Command

                result = DataReaderToEntity(dr);

            }
            catch (Exception exc)
            {
                // Fermeture Connection 
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        private VilleEntity DataReaderToEntity(IDataReader dr)
        {
            VilleEntity ville = new VilleEntity();

            ville.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            ville.Nom = dr.GetString(dr.GetOrdinal("libelle_ville"));
            ville.CodePostal = dr.GetString(dr.GetOrdinal("code_postal"));


            return ville;
        }

    }
}
