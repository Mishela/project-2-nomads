﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypePartenaireEntity
    {
        public int IdTypePartenaire { get; set; }
        public string Libelle { get; set; }
    }
}
