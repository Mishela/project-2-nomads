﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TransactionEntity
    {
        public int IdCommande { get; set; }
        public int Quantite { get; set; }
        public DateTime DateAchat { get; set; }
        public int? IdMasseur { get; set; }
        public int? IdTypePack { get; set; }
    }
}
