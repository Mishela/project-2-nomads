﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypeRecurrenceEntity
    {
        public int IdTypeRecurrence { get; set; }
        public string LibelleRecurrence { get; set; }
        public int PasRecurrence { get; set; }
        public int Duree { get; set; }// a vérifier le int car il y a aussi DateTimeOffSet pour les durée ou DateTime
    }
}
