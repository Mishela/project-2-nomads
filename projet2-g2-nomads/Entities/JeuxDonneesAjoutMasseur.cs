﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class JeuxDonneesAjoutMasseur
    {
        public MasseurEntity DemoMasseurHomme()
        {
            MasseurEntity demoMasseurHomme = new MasseurEntity();
            demoMasseurHomme.Nom = "CHARLUTEAU";
            demoMasseurHomme.Prenom = "Martin";
            demoMasseurHomme.NumeroTelephone = "06.32.54.12.78";
            string date = "12/02/1986";
            DateTime dateNaissance;
            DateTime.TryParse(date, out dateNaissance);
            demoMasseurHomme.DateNaissance = dateNaissance;
            demoMasseurHomme.Adresse = "32 bd Ornano";
            demoMasseurHomme.Desinscription = DateTime.Now;
            demoMasseurHomme.IdSexe = 1;
            demoMasseurHomme.IdVille = 1;


            return demoMasseurHomme;

        }
        public MasseurEntity DemoMasseurFemme()
        {
            MasseurEntity demoMasseurFemme = new MasseurEntity();
            demoMasseurFemme.Nom = "DUMONT";
            demoMasseurFemme.Prenom = "Anne-Marie";
            demoMasseurFemme.NumeroTelephone = "07.25.75.34.56";
            string date = "23/07/1990";
            DateTime dateNaissance;
            DateTime.TryParse(date, out dateNaissance);
            demoMasseurFemme.DateNaissance = dateNaissance;
            demoMasseurFemme.Adresse = "30 Grande rue Charles de Gaulle";
            demoMasseurFemme.Desinscription = DateTime.Now;
            demoMasseurFemme.IdSexe = 2;
            demoMasseurFemme.IdVille = 7;


            return demoMasseurFemme;

        }
    }
}
