﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class VilleEntity
    {
        public int IdVille { get; set; }
        public string Nom { get; set; }
        public string CodePostal { get; set; }
        public int? IdVilleIntervention { get; set; }
        public int? IdMasseur { get; set; }
        public int? IdPartenaire { get; set; }
        public int? IdPrestation { get; set; }

        public VilleEntity()
        {

        }
        public VilleEntity(int id, string libelle)
        {
            IdVille = id;
            Nom = libelle;
           
        }
        public VilleEntity(int id, string libelle, string cp)
        {
            IdVille = id;
            Nom = libelle;
            CodePostal = cp;
        }
    }
}
