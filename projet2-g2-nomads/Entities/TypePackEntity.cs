﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypePackEntity
    {
        public int IdPack { get; set; }
        public string Libelle { get; set; }
        public int QuantiteMads { get; set; }
        public int Prix { get; set; }
        public int? IdTransaction { get; set; }

    }
}
