﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class SexeBU
    {
        public List<SexeEntity> GetListSexe()
        {
            SexeDAO dao = new SexeDAO();
            return dao.GetAllSexe();
        }

        public SexeEntity GetSexe(int id)
        {
            SexeDAO dao = new SexeDAO();
            return dao.GetById(id);
        }
    }
}
