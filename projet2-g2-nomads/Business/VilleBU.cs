﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class VilleBU
    {
        public List<VilleEntity> GetListVille()
        {
            VilleDAO dao = new VilleDAO();
            return dao.GetAllVilles();
        }
        public VilleEntity GetByIdVille(int id)
        {
            VilleDAO dao = new VilleDAO();
            return dao.GetByIdVille(id);

        }
    }
}
