﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class PrefTravailBU
    {
        public void AjouterListPrefTravail(List<PreferenceTravailEntity> preferences) // Cette Méthode était prévu pour un GridForm que je n'ai pas encore développé
        {
            PreferenceTravailDAO dao = new PreferenceTravailDAO();
            dao.Insert(preferences);
        }
        public void AjouterPrefTravail(PreferenceTravailEntity newPreferences) // Cette Méthode était prévu pour un GridForm que je n'ai pas encore développé
        {
            PreferenceTravailDAO dao = new PreferenceTravailDAO();
            // On vérifie si la préférence de travail existe déjà
            bool inDatabase = false;

            /*foreach(PreferenceTravailEntity pT in dao.GetAllPreferenceTravail())
            {
                if(pT.IdMasseur == newPreferences.IdMasseur && pT.IdJourType == newPreferences.IdJourType)
            }*/
            dao.Insert(newPreferences);
        }

        public DataTable AffichagePrefsMasseur(int id)
        {
            PreferenceTravailDAO dao = new PreferenceTravailDAO();
            return dao.GetAllPrefMasseur(id);
        }

        public List<PreferenceTravailEntity> GetMasseurPrefTravail(int id)
        {
            PreferenceTravailDAO dao = new PreferenceTravailDAO();
            return dao.GetPrefsTravailMasseur(id);
        }


        public List<JourTypeEntity> GetListJour()
        {
            PreferenceTravailDAO dao = new PreferenceTravailDAO();
            return dao.GetAllJourTypes();
        }
    }
}
