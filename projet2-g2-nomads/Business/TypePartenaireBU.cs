﻿using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Nomads.DataAccess;

namespace AFCEPF.AI107.Nomads.Business
{
    public class TypePartenaireBU
    {
        public List<TypePartenaireEntity> GetListTypes()
        {
            TypePartenaireDAO dao = new TypePartenaireDAO();
            return dao.GetAllTypes();
        }

    }
}
