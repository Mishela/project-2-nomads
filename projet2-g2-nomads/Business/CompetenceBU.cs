﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class CompetenceBU
    {
        public List<RefCompetenceEntity> GetListCompetence()
        {
            CompetenceDAO dao = new CompetenceDAO();
            return dao.GetAllRefCompetence();
        }

        public SuiviCompetenceEntity GetLigneSuiviCompetence(int id)
        {
            CompetenceDAO dao = new CompetenceDAO();
            return dao.GetSuiviCompetenceEntityById(id);
        }

        public DataTable GetMasseurCompetences(int id)
        {
            CompetenceDAO dao = new CompetenceDAO();
            return dao.GetCompetencesMasseur(id);
        }
        
        public void ActiverCompetence(SuiviCompetenceEntity nouvelleCompetence)
        {
            bool trouve = false;
            bool novice = false;
            CompetenceDAO dao = new CompetenceDAO();
            // Verifier que la compétence n'existe pas déjà 

            foreach (SuiviCompetenceEntity s in dao.GetAllActivatedSuiviCompetenceEntity())
            {
                if(s.IdMasseur == nouvelleCompetence.IdMasseur && s.IdRefCompetence == 1 && nouvelleCompetence.IdRefCompetence != 2)
                {
                    novice = true;
                }
                if(s.IdMasseur == nouvelleCompetence.IdMasseur && s.IdRefCompetence == nouvelleCompetence.IdRefCompetence)
                {
                    trouve = true;
                }
            }

            if (!novice & !trouve)
            {
                // insert
                dao.Insert(nouvelleCompetence);
            }
            else
            {
                // Existe déjà
                if (trouve) throw new Exception("ERREUR : Compétence déjà active");
                if (novice) throw new Exception("Le masseur doit être au moins nomade !");
            }
        }

        public void DesactiverCompetence(SuiviCompetenceEntity nouvelleCompetence)
        {
            CompetenceDAO dao = new CompetenceDAO();
            dao.Update(nouvelleCompetence);

            if (nouvelleCompetence.IdRefCompetence == 1)
            {
            

                nouvelleCompetence.DateDebut = DateTime.Today;
                nouvelleCompetence.IdRefCompetence = 2;
                dao.Insert(nouvelleCompetence);

            }

        }
    }
}
