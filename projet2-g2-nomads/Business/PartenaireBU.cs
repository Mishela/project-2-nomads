﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;

namespace AFCEPF.AI107.Nomads.Business
{
    public class PartenaireBU
    {
        PartnersDAO partnersDAO = new PartnersDAO();
        PrestationDAO prestationDAO = new PrestationDAO();

        #region READ
        public List<PartenaireEntity> GetListePartenaires()
        {
            return partnersDAO.GetAllPartenaire();
        }

        public PartenaireEntity GetPartenaire(int id)
        {
            return partnersDAO.GetPartenaireById(id);
        }

        public DataTable GetDetailsPartner()
        {
            return partnersDAO.GetcompInfo();
        }

        public DataTable GetPartnerPrestations(int id)
        {
            return prestationDAO.GetAllPrestaForPartners(id);
        }
        public object GetHistoriquePartnerPrestations(int id)
        {
            return prestationDAO.GetPastPrestaForPartners(id);
        }
        #endregion

        #region ADD
        public void AjouterPartenaire(PartenaireEntity partenaire)
        {
            VerifierDoublonPartenaire(partenaire);
            partnersDAO.AddPartenaire(partenaire);
        }

        #endregion

        #region DELETE
        public void SupPartenaire(int id)
        {
            try { 
            partnersDAO.DeletePartner(id);
            }catch (Exception exp)
            {
                throw new Exception("Impossible de supprimer ce partenaire : des prestations sont à venir!");
            }
        }
        #endregion

        #region VERIFIER
        protected void VerifierDoublonPartenaire(PartenaireEntity nouveauPartenaire)
        {
            List<PartenaireEntity> listePartenaire = new List<PartenaireEntity>();
            listePartenaire = partnersDAO.GetAllPartenaire();
           
            foreach (PartenaireEntity partenaireExistant in listePartenaire)
            {
                if(partenaireExistant.DenominationSociale == nouveauPartenaire.DenominationSociale && partenaireExistant.Adresse == nouveauPartenaire.Adresse
                    && partenaireExistant.IdTypePartenaire == nouveauPartenaire.IdTypePartenaire && partenaireExistant.IdVille == nouveauPartenaire.IdVille)
                {
                    throw new Exception("Ce partenaire est déjà inscrit!");
                }
            }
        }
        #endregion
    }
}
