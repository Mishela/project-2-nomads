﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class MasseursBU
    {
        public List<MasseurEntity> GetListMasseurs()
        {
        
            MasseurDAO dao = new MasseurDAO();

            return dao.GetAllMasseur();
        }
        public List<MasseurEntity> GetListMasseursByPrestation(int idPrestation)
        {

            MasseurDAO dao = new MasseurDAO();

            return dao.GetAllMasseurByPrestation(idPrestation);
        }
        public DataTable GetListMasseursActif()
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetActivatedMasseurs();
        }
        public DataTable GetListMasseursInactif()
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetInactivatedMasseurs();
        }
        public DataTable GetListMasseursActif(string nom, string prenom, int sexe, int ville, int competence)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetActivatedMasseurs(nom, prenom, sexe, ville, competence);
        }
        public DataTable GetMasseurActif(int id)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetMasseurActiveById(id);
        }

                                    public DataTable GetMasseurInactif(int id)
                                    {
                                         MasseurDAO dao = new MasseurDAO();
                                        return dao.GetMasseurActiveById(id);
                                    }
                                    public DataTable GetMasseurByIdDataTable(int id)
                                    {
                                        MasseurDAO dao = new MasseurDAO();
                                        return dao.GetAllMasseurById(id);
                                    }


        public MasseurEntity GetMasseur(int id)
        {
            MasseurDAO dao = new MasseurDAO();
            return dao.GetById(id);
        }

        public void AjouterMasseur(MasseurEntity masseur)
        {
            VerifierDoublonMasseur(masseur);
            MasseurDAO dao = new MasseurDAO();
            dao.Insert(masseur);
        }

        public void ModifierMasseur(MasseurEntity masseur)
        {
            MasseurDAO dao = new MasseurDAO();
            dao.Update(masseur);
        }

        public void DesactiverMasseur(MasseurEntity masseur)
        {
            MasseurDAO dao = new MasseurDAO();
            dao.Desactivation(masseur);
        }

        public void ReactiverMasseur(MasseurEntity masseur)
        {
            MasseurDAO dao = new MasseurDAO();
            dao.Reactivation(masseur);
        }

        protected void VerifierDoublonMasseur (MasseurEntity masseur)
        {
            List<MasseurEntity> listeMasseur = new List<MasseurEntity>();
            MasseurDAO dao = new MasseurDAO();
            listeMasseur = dao.GetAllMasseur();
            foreach (MasseurEntity masseurExistant in listeMasseur)
            {
                if(masseurExistant.Nom==masseur.Nom && masseurExistant.Prenom==masseur.Prenom && masseurExistant.DateNaissance==masseur.DateNaissance
                   && masseurExistant.Adresse==masseur.Adresse)
                {
                    throw new Exception("Ce masseur est déjà inscrit!");
                }
            }
        }
    }
}
