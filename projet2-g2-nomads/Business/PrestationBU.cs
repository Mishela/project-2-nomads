﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AFCEPF.AI107.Nomads.Business
{
    public class PrestationBU
    {
        PrestationDAO prestationDAO = new PrestationDAO();
        MasseurDAO masseurDAO = new MasseurDAO();
        PrestationEntity prestation = new PrestationEntity();
        MasseurEntity masseur = new MasseurEntity();

        public List<PrestationEntity> GetListPrestations()
        {
            return prestationDAO.GetAllPrestation();
        }
        public List<PrestationEntity> GetListPrestationsByMasseur(int idMasseur)
        {
            return prestationDAO.GetAllInscriptionPrestationByMasseur(idMasseur);
        }
        public List<PrestationEntity> GetHistoriqueInscriptionByMasseur(int idMasseur)
        {
            return prestationDAO.GetHistoriqueInscriptionByMasseur(idMasseur);
        }
        public List<InscriptionPrestationEntity> GetListHistoriqueInscriptionsByMasseur(int idMasseur)
        {
            return prestationDAO.GetListHistoriqueInscriptionsByMasseur(idMasseur);
        }
        public PrestationEntity GetPrestationById (int id)
        {
            return prestationDAO.GetPrestationById(id);
        }

        public List<PrestationEntity> RechercherPresta(string nom, int type )
        {
            return prestationDAO.GetRecherche(nom, type);
        }

        public void AjouterTournee(PrestationEntity nouvelleTournee)
        {
                VerifierNbMasseurMin(nouvelleTournee);
                VerifierNombreMasseurs(nouvelleTournee);
                VerifierDatePrestation(nouvelleTournee);
                VerifierDoublonPrestation(nouvelleTournee);
                prestationDAO.AjouterTournee(nouvelleTournee);
        }

        public void InscriptionPrestation(InscriptionPrestationEntity nouvelleInscription)
        {
            int idMasseur = nouvelleInscription.IdMasseur;
            VerifierActifMasseur(idMasseur);
            VerifierNombreDePlace(nouvelleInscription);
            VerifierNbMadsMasseurTournee(nouvelleInscription, idMasseur);
            VerifierDateInscriptionMasseur(nouvelleInscription, idMasseur);
            
            bool trouve = false;
            // parcourir les inscriptions
            foreach (InscriptionPrestationEntity Inscription in prestationDAO.GetListAllInscriptionPrestationEntities())
            {
                if (Inscription.IdMasseur == nouvelleInscription.IdMasseur && Inscription.IdPrestation == nouvelleInscription.IdPrestation)
                {
                    trouve = true;
                }
            }
            
            if (!trouve)
            {
                masseur = masseurDAO.GetById(idMasseur);
                prestation = prestationDAO.GetPrestationById(nouvelleInscription.IdPrestation);
                int coutMads = prestation.CoutMads;
                int soldeMads = masseur.NombreMads - coutMads;
                masseur.NombreMads = soldeMads;

                // inserer une inscription
                this.prestationDAO.InscriptionPrestation(nouvelleInscription);
                this.prestationDAO.SoustraireMads(masseur);
            }
            else
            {
                throw new Exception("Masseur déjà inscrit pour cette prestation.");
            }
        }

        public void ModifierTournee(PrestationEntity tourneeModifiee)
        {
            VerifierNbMasseurMin(tourneeModifiee);
            VerifierNombreMasseurs(tourneeModifiee);
            VerifierDatePrestation(tourneeModifiee);
            VerifierDoublonPrestation(tourneeModifiee);
            try
            { 
                prestationDAO.ModifierPrestation(tourneeModifiee);
            }
            catch (Exception exp)
            {
                throw new Exception("Veuillez vérifier votre saisie.");
            }
        }

        public void DesinscriptionPrestation(int idMasseur, int idPrestation)
        {
            masseur = masseurDAO.GetById(idMasseur);
            prestation = prestationDAO.GetPrestationById(idPrestation);
            int coutMads = prestation.CoutMads;
            int soldeMads = masseur.NombreMads + coutMads;
            masseur.NombreMads = soldeMads;
            prestationDAO.DesinscriptionPrestation(idMasseur, idPrestation);
            prestationDAO.RembourserMads(masseur);
        }

        private void VerifierDatePrestation(PrestationEntity prestation)
        {
            if (prestation.Date <= DateTime.Now)
            {  
               throw new Exception("La date de début doit être postérieure à la date actuelle.");
            }
        }
        private void VerifierDoublonPrestation(PrestationEntity prestation)
        {
            List<PrestationEntity> toutesPrestations = new List<PrestationEntity>();
            toutesPrestations = GetListPrestations();
            foreach (PrestationEntity presta in toutesPrestations)
            {
                if(presta.Libelle==prestation.Libelle && presta.Date==prestation.Date && 
                    presta.Adresse==prestation.Adresse && presta.IdVille==prestation.IdVille )
                {
                    throw new Exception("Cette prestation existe déjà.");
                }
            }
        }
        private void VerifierNbMasseurMin(PrestationEntity prestation)
        {
            if(prestation.MasseurMin ==0)
            {
                throw new Exception("Attention! Le nombre de masseurs minimum est de 2.");
            }
        }

        protected void VerifierNbMadsMasseurTournee(InscriptionPrestationEntity tournee, int IdMasseur)
        {
            masseur = masseurDAO.GetById(IdMasseur);
            if (masseur.NombreMads < tournee.CoutMads)
            {
                throw new Exception("Le solde de Mads est insuffisant pour l'inscription à cette tournée.");
            }else
            {
                masseur.NombreMads = masseur.NombreMads - tournee.CoutMads;
                int nbMads = masseur.NombreMads;
                masseurDAO.SoustractionMads(IdMasseur, nbMads);
            }
        }

        protected void VerifierDateInscriptionMasseur (InscriptionPrestationEntity tournee, int idMasseur)
        {
            List<PrestationEntity> listePresta = new List<PrestationEntity>();
            listePresta = GetListPrestationsByMasseur(idMasseur);
            foreach (PrestationEntity prestation in listePresta)
            {
                if(prestation.Date == tournee.DatePrestation && prestation.IdPrestation != tournee.IdPrestation)
                {
                    throw new Exception("Vous ne pouvez pas vous inscrire à plus d'une prestation ayant la même date de début.");
                }
            }
            if (tournee.DatePrestation < DateTime.Now)
            {
                throw new Exception("Vous ne pouvez pas vous inscrire à une prestation échue ou en cours.");
            }
        }

        protected void VerifierActifMasseur (int idMasseur)
        {
            masseur = masseurDAO.GetById(idMasseur);
            if(!masseur.Actif)
            {
                throw new Exception("Ce masseur est désactivé. Il ne peut pas être inscrit!");
            }
        }

        protected void VerifierNombreMasseurs (PrestationEntity presta)
        {
            if(presta.MasseurMax<presta.MasseurMin)
            {
                throw new Exception("Le nombre de masseurs maximum ne peut être inférieur au nombre de masseur minimum!");
            }
        }
        protected void VerifierNombreDePlace (InscriptionPrestationEntity presta)
        {
            int idPrestation = presta.IdPrestation;
            PrestationEntity prestationEnCours = new PrestationEntity();
            prestationEnCours = prestationDAO.GetPrestationById(idPrestation);
        }
    }

}