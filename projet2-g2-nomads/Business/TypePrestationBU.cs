﻿using AFCEPF.AI107.Nomads.DataAccess;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Business
{
    public class TypePrestationBU
    {
        public List<TypePrestationEntity> GetListTypePrestation()
        {
            TypePrestationDAO dao = new TypePrestationDAO();
            return dao.GetAllTypePrestation();
        }

    }
}
